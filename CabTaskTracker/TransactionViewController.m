//
//  TransactionViewController.m
//  CabTaskTracker
//
//  Created by Navdeep  Singh on 8/12/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import "TransactionViewController.h"


#import "TableMenuHelpers.h"
#import "CloudEftposSDK/CloudEftposSDK.h"

#pragma clang diagnostic ignored "-Warc-performSelector-leaks"

#if TRUE // TEST MODE
// This causes transactions to be done in training mode. No messages will be sent to the test host
NSString * const email = @"test";
NSString * const password = @"test";
#else
// This will set the SDK into dev mode - messages will go to a dev host
NSString * const email = @"tomg@questps.com.au";
NSString * const password = @"questSW1!";
#endif

@interface TransactionViewController ()

@property (nonatomic, strong) CloudEftpos *cloudEftpos;

@property (nonatomic, strong) NSMutableArray *menuItems;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) UIAlertView *alertView;

@end

@implementation TransactionViewController

@synthesize cloudEftpos;
@synthesize menuItems;
@synthesize tableView;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    // Populate menu table
    self.menuItems = [[NSMutableArray alloc] init];
    
    TableMenuSection *ms = nil;
    TableMenuItem *mi = nil;
    
    ms = [[TableMenuSection alloc] init];
    ms.name = @"Start Transaction (#{POSREF})";
    mi = [[TableMenuItem alloc] init];
    mi.text = @"Purchase for $40";
    mi.imagePath = nil;
    mi.selector = @selector(startTransaction);
    [ms.items addObject:mi];
    mi = [[TableMenuItem alloc] init];
    mi.text = @"Refund for $40";
    mi.imagePath = nil;
    mi.selector = @selector(startTransactionRefund);
    [ms.items addObject:mi];
    mi = [[TableMenuItem alloc] init];
    mi.text = @"Purchase with POS Data";
    mi.imagePath = nil;
    mi.selector = @selector(startTransactionPOSData);
    [ms.items addObject:mi];
    mi = [[TableMenuItem alloc] init];
    mi.text = @"Purchase with Custom Display";
    mi.imagePath = nil;
    mi.selector = @selector(startTransactionCustom);
    [ms.items addObject:mi];
    [menuItems addObject:ms];
    
    ms = [[TableMenuSection alloc] init];
    ms.name = @"Transaction Recovery";
    mi = [[TableMenuItem alloc] init];
    mi.text = @"Recover Transaction";
    mi.imagePath = nil;
    mi.selector = @selector(recoverTransaction);
    [ms.items addObject:mi];
    mi = [[TableMenuItem alloc] init];
    mi.text = @"Recover Transaction (No Amount)";
    mi.imagePath = nil;
    mi.selector = @selector(recoverTransactionNoAmount);
    [ms.items addObject:mi];
    mi = [[TableMenuItem alloc] init];
    mi.text = @"Recover Transaction (No Reference)";
    mi.imagePath = nil;
    mi.selector = @selector(recoverTransactionLast);
    [ms.items addObject:mi];
    [menuItems addObject:ms];
    
    ms = [[TableMenuSection alloc] init];
    ms.name = @"Sleep States";
    mi = [[TableMenuItem alloc] init];
    mi.text = @"Sleep microPay";
    mi.imagePath = nil;
    mi.selector = @selector(sleep);
    [ms.items addObject:mi];
    mi = [[TableMenuItem alloc] init];
    mi.text = @"Wake microPay";
    mi.imagePath = nil;
    mi.selector = @selector(wake);
    [ms.items addObject:mi];
    [menuItems addObject:ms];
    
    ms = [[TableMenuSection alloc] init];
    ms.name = @"Pairing";
    mi = [[TableMenuItem alloc] init];
    mi.text = @"Pair new microPay";
    mi.imagePath = nil;
    mi.selector = @selector(pairDevice);
    [ms.items addObject:mi];
    [menuItems addObject:ms];
    
    ms = [[TableMenuSection alloc] init];
    ms.name = @"Advanced";
    mi = [[TableMenuItem alloc] init];
    mi.text = @"Advanced Menu";
    mi.imagePath = nil;
    mi.selector = @selector(showAdvancedMenu);
    [ms.items addObject:mi];
    mi = [[TableMenuItem alloc] init];
    mi.text = @"Check for updates";
    mi.imagePath = nil;
    mi.selector = @selector(updatePINpad);
    [ms.items addObject:mi];
    [menuItems addObject:ms];
    
    ms = [[TableMenuSection alloc] init];
    ms.name = @"Debug Auth";
    mi = [[TableMenuItem alloc] init];
    mi.text = @"Auth User";
    mi.imagePath = nil;
    mi.selector = @selector(authenticate);
    [ms.items addObject:mi];
    mi = [[TableMenuItem alloc] init];
    mi.text = @"Verify Password";
    mi.imagePath = nil;
    mi.selector = @selector(verifyPass);
    [ms.items addObject:mi];
    mi = [[TableMenuItem alloc] init];
    mi.text = @"Verify Bad Password";
    mi.imagePath = nil;
    mi.selector = @selector(verifyPassWrong);
    [ms.items addObject:mi];
    mi = [[TableMenuItem alloc] init];
    mi.text = @"Auth POS";
    mi.imagePath = nil;
    mi.selector = @selector(authorisePOS);
    [ms.items addObject:mi];
    [menuItems addObject:ms];
    
    
    [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(createEftposObject) userInfo:Nil repeats:false];
    
    /*
     // Initialise the CloudEftpos Library with a token
     NSData *token = ...;
     cloudEftpos = [[CloudEftpos alloc] initWithToken:token];
     
     
     // Free the library and disconnect any PINpad
     eppMobile = nil;
     */
}

-(void) createEftposObject
{
    // Initialise the CloudEftpos Library and reconnect to the last PINpad if possible
    cloudEftpos = [[CloudEftpos alloc] init];
    
    /*
     Optionally theme the SDK
     UIColor *blue = [UIColor colorWithRed:3/255.0f green:119/255.0f blue:213/255.0f alpha:1.0f];
     UIColor *darkBlue = [UIColor colorWithRed:0/255.0f green:99/255.0f blue:180/255.0f alpha:1.0f];
     [cloudEftpos setNavigationBarColor:blue];
     [cloudEftpos setNavigationBarTitleColor:[UIColor whiteColor]];
     [cloudEftpos setNavigationBarTitleShadowColor:darkBlue];
     [cloudEftpos setNavigationBarTintColor:[UIColor whiteColor]];
     [cloudEftpos setStatusBarStyle:UIStatusBarStyleLightContent];
     */
    
    [cloudEftpos registerForStatusChangesWithBlock:^(NSDictionary *status){
        NSLog(@"%@", status);
    }];
    
    // At this point we MUST check if we are recovering from a power failure or crash
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    bool transactionInProgress = [(NSNumber *)[prefs valueForKey:@"transactionInProgress"] boolValue];
    if (transactionInProgress)
    {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Transaction Recovery"
                              message:@"The phone crashed or lost power during a transaction."
                              delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
        // If we stored a transaction that had not been completed, we can continue it (get the result) by using
        // the recoverTransaction method
        // See the examples below for how to use the recoverTransaction method
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showAdvancedMenu
{
    // Display the advanced menu
    [cloudEftpos advancedMenuWithPresentingViewController
     :self
     animated:true
     onClose:^(void) {
         // Advanced menu closed
     }
     onPrint:^(NSString *type, NSString *receipt) {
         NSLog(@"Print receipt of type: %@\n %@", type, receipt);
     }];
}

- (void)startTransaction
{
    // Get POS reference
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSNumber *transCount = [prefs valueForKey:@"transCount"];
    if (transCount == nil)
    {
        transCount = [NSNumber numberWithInt:0];
    }
    transCount = [NSNumber numberWithInt:([transCount intValue] + 1)];
    [prefs setValue:transCount forKey:@"transCount"];
    
    // Add sequence number (different sequence number for different transactions in the one tender)
    int sequenceNumber = 1;
    NSString *posReference = [NSString stringWithFormat:@"%d%d", [transCount intValue], sequenceNumber];
    
    // Create TransactionRequest for $40
    TransactionRequest *tr = [[TransactionRequest alloc] init];
    [tr setAmount:4000];
    [tr setCashout:0];
    [tr setTipamount:0];
    [tr setTransactionType:Purchase];
    [tr setPosReference:posReference];
    
    // Enter transaction state
    [prefs setValue:posReference forKey:@"lastPosReference"];
    [prefs setValue:[NSNumber numberWithBool:YES] forKey:@"transactionInProgress"];
    // Store any other details for this transaction that will be required if there is a power failure or crash
    // This MUST be saved, otherwise there is a risk we could lose a transaction
    if (![prefs synchronize])
    {
        // Error, unable to save transaction state
        return;
    }
    
    // Start transaction
    TransactionViewController * __weak weakSelf = self;
    [cloudEftpos startTransaction:tr
     withPresentingViewController:self
                         animated:true
                     onCompletion:^(NSDictionary *results, NSError *error) {
                         if (error) {
                             NSLog(@"Error with transaction: %@", [error localizedDescription]);
                             
                             UIAlertView *errorAlert = [[UIAlertView alloc]
                                                        initWithTitle: @"Error"
                                                        message: [error localizedDescription]
                                                        delegate:self
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles:nil];
                             [errorAlert show];
                             return;
                         }
                         // Extract data from result
                         bool approved = [(NSNumber *)[results valueForKey:@"approved"] boolValue];
                         NSString *responseCode = [results valueForKey:@"responseCode"];
                         NSString *responseText = [results valueForKey:@"responseText"];
                         // Do something with result...
                         NSLog(@"%@", results);
                         // Remove transaction state
                         [prefs setValue:[NSNumber numberWithBool:NO] forKey:@"transactionInProgress"];
                         [weakSelf.tableView reloadData];
                         
                         UIImage *image = [results valueForKey:@"signatureImage"];
                         if (image != nil)
                         {
                             // Convert UIImage to JPEG
                             NSData *imgData = UIImageJPEGRepresentation(image, 1); // 1 is compression quality
                             
                             // Identify the home directory and file name
                             NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/Test.jpg"];
                             
                             // Write the file.  Choose YES atomically to enforce an all or none write. Use the NO flag if partially written files are okay which can occur in cases of corruption
                             [imgData writeToFile:jpgPath atomically:YES];
                         }
                     }];
}

- (void)startTransactionPOSData
{
    // Get POS reference
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSNumber *transCount = [prefs valueForKey:@"transCount"];
    if (transCount == nil)
    {
        transCount = [NSNumber numberWithInt:0];
    }
    transCount = [NSNumber numberWithInt:([transCount intValue] + 1)];
    [prefs setValue:transCount forKey:@"transCount"];
    
    // Add sequence number (different sequence number for different transactions in the one tender)
    int sequenceNumber = 1;
    NSString *posReference = [NSString stringWithFormat:@"%d%d", [transCount intValue], sequenceNumber];
    
    // Create TransactionRequest for $40
    TransactionRequest *tr = [[TransactionRequest alloc] init];
    [tr setAmount:4000];
    [tr setCashout:0];
    [tr setTipamount:0];
    [tr setTransactionType:Purchase];
    [tr setPosReference:posReference];
    [tr setPosData1:[@"fZYQXE6kqDWwrtKQ9G3hTjfIhw-WZrjrBD5LtiYYtq-BmDJOWx7YfsnaExYZljbw_UP91Uomgf4ftMxQ1GDHAohRD9KzbSVpWU0OWIxjnqSyFGe9qIa8I8Dc" dataUsingEncoding:NSUTF8StringEncoding]];
    [tr setPosData2:[@"fySolAyHShPp8f8-L9UuI9H3VUePvahqmILeEWylrCCbmxjzOlCxnJMFHM0T8gp8XO8q4Vu6V1uqS-NiQ-Xf9JjF6bTKw4o8RS1p575t5rxB483Jv_OKC99J5zbJ6bI3nvPDGsYtDktMYKdDAC2LGqpROG6XmeYkEvqpBcjG4a3Xsedk8nzHm9yuNfMCt3JQ0nqqMEvWAQU" dataUsingEncoding:NSUTF8StringEncoding]];
    
    // Enter transaction state
    [prefs setValue:posReference forKey:@"lastPosReference"];
    [prefs setValue:[NSNumber numberWithBool:YES] forKey:@"transactionInProgress"];
    // Store any other details for this transaction that will be required if there is a power failure or crash
    // This MUST be saved, otherwise there is a risk we could lose a transaction
    if (![prefs synchronize])
    {
        // Error, unable to save transaction state
        return;
    }
    
    // Start transaction
    TransactionViewController * __weak weakSelf = self;
    [cloudEftpos startTransaction:tr
     withPresentingViewController:self
                         animated:true
                     onCompletion:^(NSDictionary *results, NSError *error) {
                         if (error) {
                             NSLog(@"Error with transaction: %@", [error localizedDescription]);
                             
                             UIAlertView *errorAlert = [[UIAlertView alloc]
                                                        initWithTitle: @"Error"
                                                        message: [error localizedDescription]
                                                        delegate:self
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles:nil];
                             [errorAlert show];
                             return;
                         }
                         // Extract data from result
                         bool approved = [(NSNumber *)[results valueForKey:@"approved"] boolValue];
                         NSString *responseCode = [results valueForKey:@"responseCode"];
                         NSString *responseText = [results valueForKey:@"responseText"];
                         // Do something with result...
                         NSLog(@"%@", results);
                         // Remove transaction state
                         [prefs setValue:[NSNumber numberWithBool:NO] forKey:@"transactionInProgress"];
                         [weakSelf.tableView reloadData];
                     }];
}

- (void) startTransactionCustom
{
    
    // Get POS reference
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSNumber *transCount = [prefs valueForKey:@"transCount"];
    if (transCount == nil)
    {
        transCount = [NSNumber numberWithInt:0];
    }
    transCount = [NSNumber numberWithInt:([transCount intValue] + 1)];
    [prefs setValue:transCount forKey:@"transCount"];
    
    // Add sequence number (different sequence number for different transactions in the one tender)
    int sequenceNumber = 1;
    NSString *posReference = [NSString stringWithFormat:@"%d%d", [transCount intValue], sequenceNumber];
    
    // Create TransactionRequest for $40
    TransactionRequest *tr = [[TransactionRequest alloc] init];
    [tr setAmount:4000];
    [tr setCashout:0];
    [tr setTipamount:0];
    [tr setTransactionType:Purchase];
    [tr setPosReference:posReference];
    
    // Enter transaction state
    [prefs setValue:posReference forKey:@"lastPosReference"];
    [prefs setValue:[NSNumber numberWithBool:YES] forKey:@"transactionInProgress"];
    // Store any other details for this transaction that will be required if there is a power failure or crash
    // This MUST be saved, otherwise there is a risk we could lose a transaction
    if (![prefs synchronize])
    {
        // Error, unable to save transaction state
        return;
    }
    
    // Start transaction
    TransactionViewController * __weak weakSelf = self;
    [cloudEftpos startTransaction:tr
                     displayBlock:^(NSString *msg) {
                         if (self.alertView != nil) {
                             [self.alertView dismissWithClickedButtonIndex:-1 animated:NO];
                         }
                         self.alertView = [[UIAlertView alloc] initWithTitle:@"Display"
                                                                     message:msg
                                                                    delegate:self
                                                           cancelButtonTitle:@"Cancel"
                                                           otherButtonTitles:nil];
                         [self.alertView show];
                     } promptBlock:nil
             signaturePromptBlock:nil
     withPresentingViewController:self
                         animated:true
                     onCompletion:^(NSDictionary *results, NSError *error) {
                         if (self.alertView != nil) {
                             [self.alertView dismissWithClickedButtonIndex:-1 animated:NO];
                         }
                         if (error) {
                             NSLog(@"Error with transaction: %@", [error localizedDescription]);
                             
                             UIAlertView *errorAlert = [[UIAlertView alloc]
                                                        initWithTitle: @"Error"
                                                        message: [error localizedDescription]
                                                        delegate:self
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles:nil];
                             [errorAlert show];
                             return;
                         }
                         // Extract data from result
                         bool approved = [(NSNumber *)[results valueForKey:@"approved"] boolValue];
                         NSString *responseCode = [results valueForKey:@"responseCode"];
                         NSString *responseText = [results valueForKey:@"responseText"];
                         // Do something with result...
                         NSLog(@"%@", results);
                         // Remove transaction state
                         [prefs setValue:[NSNumber numberWithBool:NO] forKey:@"transactionInProgress"];
                         [weakSelf.tableView reloadData];
                         
                         UIImage *image = [results valueForKey:@"signatureImage"];
                         if (image != nil)
                         {
                             // Convert UIImage to JPEG
                             NSData *imgData = UIImageJPEGRepresentation(image, 1); // 1 is compression quality
                             
                             // Identify the home directory and file name
                             NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/Test.jpg"];
                             
                             // Write the file.  Choose YES atomically to enforce an all or none write. Use the NO flag if partially written files are okay which can occur in cases of corruption
                             [imgData writeToFile:jpgPath atomically:YES];
                         }
                     }];
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [cloudEftpos cancelTransaction];
}

- (void)startTransactionRefund
{
    // Get POS reference
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSNumber *transCount = [prefs valueForKey:@"transCount"];
    if (transCount == nil)
    {
        transCount = [NSNumber numberWithInt:0];
    }
    transCount = [NSNumber numberWithInt:([transCount intValue] + 1)];
    [prefs setValue:transCount forKey:@"transCount"];
    
    // Add sequence number (different sequence number for different transactions in the one tender)
    int sequenceNumber = 1;
    NSString *posReference = [NSString stringWithFormat:@"%d%d", [transCount intValue], sequenceNumber];
    
    // Create TransactionRequest for $40 refund
    TransactionRequest *tr = [[TransactionRequest alloc] init];
    [tr setAmount:4000];
    [tr setCashout:0];
    [tr setTipamount:0];
    [tr setTransactionType:Refund];
    [tr setPosReference:posReference];
    
    // Enter transaction state
    [prefs setValue:posReference forKey:@"lastPosReference"];
    [prefs setValue:[NSNumber numberWithBool:YES] forKey:@"transactionInProgress"];
    // Store any other details for this transaction that will be required if there is a power failure or crash
    // This MUST be saved, otherwise there is a risk we could lose a transaction
    if (![prefs synchronize])
    {
        // Error, unable to save transaction state
        return;
    }
    
    // Start transaction
    TransactionViewController * __weak weakSelf = self;
    [cloudEftpos startTransaction:tr
     withPresentingViewController:self
                         animated:true
                     onCompletion:^(NSDictionary *results, NSError *error) {
                         if (error) {
                             NSLog(@"Error with transaction: %@", [error localizedDescription]);
                             
                             UIAlertView *errorAlert = [[UIAlertView alloc]
                                                        initWithTitle: @"Error"
                                                        message: [error localizedDescription]
                                                        delegate:self
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles:nil];
                             [errorAlert show];
                             return;
                         }
                         // Extract data from result
                         bool approved = [(NSNumber *)[results valueForKey:@"approved"] boolValue];
                         NSString *responseCode = [results valueForKey:@"responseCode"];
                         NSString *responseText = [results valueForKey:@"responseText"];
                         // Do something with result...
                         NSLog(@"%@", results);
                         // Remove transaction state
                         [prefs setValue:[NSNumber numberWithBool:NO] forKey:@"transactionInProgress"];
                         [weakSelf.tableView reloadData];
                     }];
}

-(void) sleep
{
    [cloudEftpos sleepDevice];
}

-(void) wake
{
    [cloudEftpos wakeDevice];
}

- (void)pairDevice
{
    // Pair to PINpad
    [cloudEftpos pairDeviceWithPresentingViewController:self
                                               animated:true
                                           onCompletion:^(NSError *error) {
                                               if (error) {
                                                   NSLog(@"Error pairing to PINpad: %@", [error localizedDescription]);
                                                   return;
                                               }
                                               // PINpad is now paired - a status update should trigger to
                                               // indicate a "connected" state
                                           }];
}

- (void)recoverTransaction
{
    // Example using the last known POS reference with an amount and transaction type
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *posReference = [prefs valueForKey:@"lastPosReference"];
    
    [cloudEftpos recoverTransactionWithPosReference
     :posReference
     transactionType:Purchase
     amount:4000
     withPresentingViewController:self
     animated:true
     onCompletion:^(NSDictionary *result, NSError *error) {
         if (error) {
             NSLog(@"Error with recover transaction: %@", [error localizedDescription]);
             return;
         }
         NSNumber *approved = (NSNumber *)[result objectForKey:@"approved"];
         if (approved == nil)
         {
             NSLog(@"No transaction matched the given criteria");
             return;
         }
         NSString *receipt = (NSString *)[result objectForKey:@"receipt"];
         UIImage *signature = (UIImage *)[result objectForKey:@"signature"];
         
         // Do something with receipt and signature
         NSLog(@"%@", result);
         // Remove pending transaction
         [prefs setValue:[NSNumber numberWithBool:NO] forKey:@"transactionInProgress"];
         [prefs synchronize];
     }];
}

- (void)recoverTransactionNoAmount
{
    // Example using the last known POS reference with a transaction type and unknown amount
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *posReference = [prefs valueForKey:@"lastPosReference"];
    
    [cloudEftpos recoverTransactionWithPosReference
     :posReference
     transactionType:Purchase
     amount:0
     withPresentingViewController:self
     animated:true
     onCompletion:^(NSDictionary *result, NSError *error) {
         if (error) {
             NSLog(@"Error with recover transaction: %@", [error localizedDescription]);
             return;
         }
         NSNumber *approved = (NSNumber *)[result objectForKey:@"approved"];
         if (approved == nil)
         {
             NSLog(@"No transaction matched the given criteria");
             return;
         }
         NSString *receipt = (NSString *)[result objectForKey:@"receipt"];
         UIImage *signature = (UIImage *)[result objectForKey:@"signature"];
         
         // Do something with receipt and signature
         NSLog(@"%@", result);
         // Remove pending transaction
         [prefs setValue:[NSNumber numberWithBool:NO] forKey:@"transactionInProgress"];
         [prefs synchronize];
     }];
}

- (void)recoverTransactionLast
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    // Example getting the last transaction with no known details
    [cloudEftpos recoverTransactionWithPosReference
     :nil
     transactionType:0
     amount:0
     withPresentingViewController:self
     animated:true
     onCompletion:^(NSDictionary *result, NSError *error) {
         if (error) {
             NSLog(@"Error with recover transaction: %@", [error localizedDescription]);
             return;
         }
         NSNumber *approved = (NSNumber *)[result objectForKey:@"approved"];
         if (approved == nil)
         {
             NSLog(@"No transaction matched the given criteria");
             return;
         }
         NSString *receipt = (NSString *)[result objectForKey:@"receipt"];
         UIImage *signature = (UIImage *)[result objectForKey:@"signature"];
         
         // Do something with receipt and signature
         NSLog(@"%@", result);
         // Remove pending transaction
         [prefs setValue:[NSNumber numberWithBool:NO] forKey:@"transactionInProgress"];
         [prefs synchronize];
     }];
}

- (void)updatePINpad
{
    // Update the PINpad
    [cloudEftpos updatePINpadWithPresentingViewController
     :self
     animated:true
     onCompletion:^(NSError *error) {
         if (error) {
             NSLog(@"Error updating PINpad: %@", [error localizedDescription]);
             return;
         }
         // Do something with version
     }];
}

/* Authentication */
-(void) simpleAuthentication
{
    // This shows how to quickly authenticate
    [cloudEftpos verifyCredentials:email password:password onCompletion:^(bool verified, NSError *error) {
        if (!verified)
        {
            // Cannot proceed. Should warn the user and try again later
            NSLog(@"Error verifying credentials: %@", [error localizedDescription]);
            return;
        }
        
        NSString *posName = @"unique POS Name";
        [cloudEftpos authorisePOS:posName onCompletion:^(bool authorised, NSError *error) {
            if (!authorised)
            {
                // Cannot proceed. Should warn the user and try again
                NSLog(@"Error authorising POS: %@", [error localizedDescription]);
                return;
            }
            
            // Now authorised. This authorisation code should not be called again.
            NSLog(@"Verified and authorised!");
        }];
    }];
}

-(void) authenticate
{
    [cloudEftpos verifyCredentials:email password:password onCompletion:^(bool verified, NSError *error) {
        if (verified)
            NSLog(@"User verified");
        else
            NSLog(@"User invalid");
    }];
}

-(void) verifyPass
{
    bool verified = [cloudEftpos verifyPassword:password];
    if (verified)
        NSLog(@"Password verified");
    else
        NSLog(@"Password wrong");
}

-(void) verifyPassWrong
{
    bool verified = [cloudEftpos verifyPassword:@"wordpass"];
    if (verified)
        NSLog(@"Password verified");
    else
        NSLog(@"Password wrong");
}

-(void) authorisePOS
{
    [cloudEftpos authorisePOS:@"Cloud Eftpos Test" onCompletion:^(bool authorised, NSError *error) {
        if (authorised)
            NSLog(@"POS authorised");
        else
            NSLog(@"Authorisation failed");
    }];
}


// Table methods

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return [menuItems count];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *name = [(TableMenuSection *)[menuItems objectAtIndex:section] name];
    if ([name rangeOfString:@"{POSREF}"].location != NSNotFound)
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *posReference = [prefs valueForKey:@"lastPosReference"];
        if (posReference == nil)
            posReference = @"0";
        name = [name stringByReplacingOccurrencesOfString:@"{POSREF}" withString:posReference];
    }
    return name;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[(TableMenuSection *)[menuItems objectAtIndex:section] items] count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    TableMenuItem *mi = (TableMenuItem *)[[(TableMenuSection *)[menuItems objectAtIndex:[indexPath section]] items] objectAtIndex:[indexPath item]];
	cell.textLabel.text = [mi text];
    if ([mi imagePath])
    {
        cell.imageView.image = [UIImage imageWithContentsOfFile:[mi imagePath]];
    }
	cell.accessoryType = UITableViewCellAccessoryNone;
    
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TableMenuItem *mi = (TableMenuItem *)[[(TableMenuSection *)[menuItems objectAtIndex:[indexPath section]] items] objectAtIndex:[indexPath item]];
    if ([self respondsToSelector:[mi selector]])
    {
        [self performSelector:[mi selector]];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:false];
}



@end
