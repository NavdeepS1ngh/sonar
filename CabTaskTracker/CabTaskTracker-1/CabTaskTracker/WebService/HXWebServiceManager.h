//
//  HXWebServiceManager.h
//  CustomerOnAR
//
//  Created by aditi on 21/07/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "HXServiceMgrProtocol.h"

#define apiKey @"T8rqHUNFWuncssSIGtDFthVjPRHRU7dADd1n"
enum request
{
    EAUTHENTICATEUSER = 0,
    EGETVEHICLELIST,
    EGETMESSAGE,
    EPOSTMESSAGE,
    EMAINTENANCE ,
    EGETOPSDATA,
    EPOSTOPSDATA
    
};

enum errorType
{
    EOK = 200,
    EFOUND = 302,
    EBADREQUEST = 400,
    EUNAUTHORIZED = 401,
    ENOTFOUNT = 404,
    EINTERNALSERVERERROR = 500,
    
};

@interface HXWebServiceManager : NSObject<NSURLConnectionDelegate>
{
    NSMutableData* webData;
    int connectionType;
}

@property(readwrite,assign)id<HXServiceMgrProtocol>serviceDelegate;

-(void) sendAuthenticateUserRequestWithUserName:(NSString*)userName password:(NSString*)password;

-(void) getVehicleListForUserName:(NSString*)userName;

-(void)getMaintenanceRequest;

-(void)getOpsDataRequest;
-(void)postOpsDataRequest;



@end
