//
//  HXServiceMgrProtocol.h
//  CabTaskTracker
//
//  Created by Mithun on 29/07/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HXServiceMgrProtocol <NSObject>

@optional
-(void)authenticationResult:(BOOL)isSucess;

-(void)recievedVehicleList:(NSArray *)vehicleList;
-(void)recievedVehicleListFailedWithReason:(NSString *)reason;

-(void)recievedMaintenanceData:(NSDictionary *)maintenanceDict;
-(void)recievedMaintenanceFailedWithReason:(NSString *)reason;

-(void)recievedOpsData:(NSDictionary *)opsDict;
-(void)recievedOpsFailedWithReason:(NSString *)reason;

@end
