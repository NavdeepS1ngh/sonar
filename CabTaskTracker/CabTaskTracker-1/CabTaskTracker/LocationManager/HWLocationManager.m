//
//  HWLocationManager.m
//  LocManager
//
//  Created by aditi on 31/01/13.
//  Copyright (c) 2013 hexaware. All rights reserved.
//

#import "HWLocationManager.h"


@implementation HWLocationManager
static  HWLocationManager* sharedInstance=nil;

@synthesize locationManager,currentLocation;

- (id) init {
    self = [super init];
    if (self != nil) {
        
        
        self.locationManager = [[CLLocationManager alloc] init] ;
        self.locationManager.delegate = self; // send loc updates to myself
        
        [self.locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
        
        amInsideRegion = NO;
        
    }
    return self;
}

+(HWLocationManager*) sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[HWLocationManager alloc] init];
        
    });
    
    return sharedInstance;
}

#pragma mark -
#pragma user defined methods

-(void)startLocationService{
 
    //Set teh distance to get the next loaction update
    //starts to get the location service
    [self.locationManager startUpdatingLocation];
    [self.locationManager setPausesLocationUpdatesAutomatically:NO];
    
    
    
}

-(void)stopLocationService{
    
    [self.locationManager stopUpdatingHeading];
    [self.locationManager stopUpdatingLocation];

    
}

#pragma mark -
#pragma Location Change delegates
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    
    
    //Get the latest geo co-ordinates
    latitude = (double) manager.location.coordinate.latitude;
    longitude = (double) manager.location.coordinate.longitude;
    altitude = (double) manager.location.altitude;
    //Updates the latest location to the caller
    
//    NSTimeInterval locationAge = -[manager.location.timestamp timeIntervalSinceNow];
//    if (locationAge < 10.0) return;
    
    
    CLLocation *loc1 = [[CLLocation alloc] initWithLatitude:currentLocation.coordinate.latitude longitude:manager.location.coordinate.longitude];
    
    CLLocation *loc2 = [[CLLocation alloc] initWithLatitude:manager.location.coordinate.latitude longitude:currentLocation.coordinate.longitude];
    
    double distance = [loc1 distanceFromLocation:loc2];
    
    currentLocation = manager.location;
    
    CLLocationSpeed speed = manager.location.speed;
    
    NSLog(@"speed at current location : %f",speed);
    
   UIAlertView* popAlert = [[UIAlertView alloc]initWithTitle:@"Speed" message:[NSString stringWithFormat:@"speed : %f",speed]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    
    [popAlert show];
    if(distance > 20)
    {
        //significant location update
    }

  
    
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
	NSLog(@"Error: %@", [error description]);
}

-(double)DEGREES_TO_RADIANS:(double)angle{
    
    return ((angle) * M_PI / 180.0);
}
-(double)RADIANS_TO_DEGREES:(double)angle{
    
    return ((angle) * 180.0/M_PI);
}

@end

