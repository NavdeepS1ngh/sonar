//
//  HWLocationManager.h
//  LocManager
//
//  Created by aditi on 31/01/13.
//  Copyright (c) 2013 hexaware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>



#define METERS_PER_MILE 1609.344
#define EARTH_RADIUS 6371000
#define CIRCLE_ANGLE 360
#define TOTAL_ANGLE 8

#define MILES_PER_KM 0.6213





@interface HWLocationManager : NSObject <CLLocationManagerDelegate> {
    
    BOOL amInsideRegion;
    
	CLLocationManager *locationManager;
    double latitude;
    double longitude;
    double altitude;
    double azimuth;
    
    CLRegion *userTrackingRegion;
}
@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic,strong)CLLocation* currentLocation;
+(HWLocationManager*) sharedInstance;

-(void)startLocationService;
-(void)stopLocationService;
-(NSMutableArray*)getMaxValueLocation;

@end
