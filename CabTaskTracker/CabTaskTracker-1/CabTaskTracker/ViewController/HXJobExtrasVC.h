//
//  HXJobExtrasVC.h
//  CabTaskTracker
//
//  Created by Mithun R on 04/07/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HXJobExtrasVC : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) NSArray *titleList;

@property (strong, nonatomic) IBOutlet UITableView *tblExtras;


- (IBAction)btnBackAction:(id)sender;
- (IBAction)btnPaymentAction:(id)sender;


@end
