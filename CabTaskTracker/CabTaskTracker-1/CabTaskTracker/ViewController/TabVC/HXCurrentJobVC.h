//
//  HXCurrentJobVC.h
//  CabTaskTracker
//
//  Created by Mithun R on 02/07/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

enum status
{
    EACCEPTED = 0,
    EONSITE,
    EPROGRESS,
    ECOMPLETED,
};

enum tableOrderList
{
    EJOBID = 0,
    EPUTIME,
    EACCOUNT,
    EPASSENGER,
    EPUADDRESS,
    EDRADDRESS,
    ESPLINSTRUCTION,
    EVEHICLETYPE,
    ELEGS
};

@interface HXCurrentJobVC : UIViewController <UIAlertViewDelegate,MFMessageComposeViewControllerDelegate,UITableViewDataSource,UITableViewDelegate>
{
  
    IBOutlet UIView *viewTransparent;
    IBOutlet UINavigationBar *navigationBar;
    IBOutlet UITableView *tblMessageTemplate;
    IBOutlet UIView *viewMessage;
    IBOutlet UIView *viewMain;
}

@property (strong, nonatomic) IBOutlet UITableView *tblCurerntJob;

@property (strong, nonatomic) IBOutlet UIView *vwIndicatorBar;
@property (strong, nonatomic) IBOutlet UIView *vwIndicatorHolder;

@property (strong, nonatomic) IBOutlet UIButton *btnAccepted;
@property (strong, nonatomic) IBOutlet UIButton *btnOnSite;
@property (strong, nonatomic) IBOutlet UIButton *btnProgress;
@property (strong, nonatomic) IBOutlet UIButton *btnCompleted;

@property int indicatorIncreaseConstant;

//for message tab to passenger ,.,only for now,.,will have to make action and outlet programaticallly afterwards
- (IBAction)btnMessage:(id)sender;
//
-(void)createTblData;
-(void)createAlertWithTitle:(NSString *)alertTitle andMessage:(NSString *)alertMessage;

- (IBAction)btnStateChangeAction:(id)sender;
- (IBAction)btnLogoutAction:(id)sender;


@end
