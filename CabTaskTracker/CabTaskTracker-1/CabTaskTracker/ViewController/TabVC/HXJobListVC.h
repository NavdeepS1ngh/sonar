//
//  HXJobListVC.h
//  ETG_App
//
//  Created by aditi on 02/07/14.
//  Copyright (c) 2014 hexaware. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STDispatchedJobCell.h"
#import "STAllJobCellTableViewCell.h"
@interface HXJobListVC : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tblJobList;

- (IBAction)btnLogoutAction:(id)sender;
@end
