//
//  HXJobListVC.m
//  ETG_App
//
//  Created by aditi on 02/07/14.
//  Copyright (c) 2014 hexaware. All rights reserved.
//

#import "HXJobListVC.h"

#import "HXJobDetails.h"
#import "HXMainModel.h"
#import "HXJobDetailsModel.h"

@interface HXJobListVC ()
{
    NSString *currentStatus;
    
    HXJobDetailsModel *jobdetail;

    NSDateFormatter *timeDateDF;
}

@end

@implementation HXJobListVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self retriveCurrentStatus];
    
    timeDateDF = [[NSDateFormatter alloc] init];
    //[timeDateDF setDateFormat:@"HH:mm:ss dd-MMM-yyyy"];
    [timeDateDF setDateFormat:@"HH:mm:ss"];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self retriveCurrentStatus];
    [_tblJobList reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int height;
    
    if ([HXMainModel sharedInstance].dispatchJobDetailsList.count>0)
    {
        if ([HXMainModel sharedInstance].dispatchJobDetailsList.count>indexPath.row) {
            height = 80;
        }
        else
        {
            height = 44;
        }
    }
    else
    {
        height = 44;
    }
    
    return height;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    STDispatchedJobCell *customCellDispatch;
    STAllJobCellTableViewCell * customCellAll;
    static NSString *CellIdentifier;

    //For seperating Dispatch Job and Normal Job Algo:
    
    if ([[HXMainModel sharedInstance]dispatchJobDetailsList].count > 0) {
        
        if ([HXMainModel sharedInstance].dispatchJobDetailsList.count>indexPath.row) {
            jobdetail = [HXMainModel sharedInstance].dispatchJobDetailsList[indexPath.row];
        }
        else
        {
            jobdetail = [HXMainModel sharedInstance].jobDetailsList[indexPath.row - [HXMainModel sharedInstance].dispatchJobDetailsList.count];
        }
    }
    else
    {
        jobdetail = [HXMainModel sharedInstance].jobDetailsList[indexPath.row];
    }
    
    //To get Passenger List Algo:
    NSString *pasengerNameDisplay;
    if (jobdetail.passengersList.count > 1)
    {
        pasengerNameDisplay = [NSString stringWithFormat:@"%@ & %i More", jobdetail.passengersList[0], jobdetail.passengersList.count];
    }
    else if (jobdetail.passengersList.count == 0)
    {
        pasengerNameDisplay = @"";
    }
    else
    {
        pasengerNameDisplay = jobdetail.passengersList[0];
    }
    
    
    if ([[HXMainModel sharedInstance]dispatchJobDetailsList].count > 0)
    {
        
        if ([HXMainModel sharedInstance].dispatchJobDetailsList.count>indexPath.row) {
            
            CellIdentifier = @"DispatchedJobCell";
            customCellDispatch = (STDispatchedJobCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if ([currentStatus isEqualToString:@"na"]) {
                [customCellDispatch setBackgroundColor:[UIColor colorWithRed:.5882 green:.5882  blue:.5882  alpha:1.0]];
                
            }
            else
            {
                [customCellDispatch setBackgroundColor:[UIColor colorWithRed:.5882 green:.6862  blue:.5882  alpha:1.0]];
                [customCellDispatch setUserInteractionEnabled:NO];
                [[customCellDispatch imgNavigateIcon]setHidden:YES];
            }
            [customCellDispatch.lblJobId setText:jobdetail.bookingNumber];
            [customCellDispatch.lblPickupTime setText:[timeDateDF stringFromDate:jobdetail.pickupTime]];
            [customCellDispatch.lblName setText:pasengerNameDisplay];
            
            NSString *address = [NSString stringWithFormat:@"%@\n%@",jobdetail.puAddress, jobdetail.puSuburb];
            [customCellDispatch.lblPickupAddress setText:address];
            
            return customCellDispatch;
        }
        else
        {
            CellIdentifier = @"AllJobCell";
            customCellAll = (STAllJobCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            [[customCellAll lblPickupPersonName]setText:pasengerNameDisplay];
            [[customCellAll lblPickupTime]setText:[timeDateDF stringFromDate:jobdetail.pickupTime]];
            [[customCellAll lblPickupAddress]setText:jobdetail.puSuburb];
            
            return customCellAll;
        }
    }
    else //Smae as inner else statement
    {
        CellIdentifier = @"AllJobCell";
        customCellAll = (STAllJobCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        [[customCellAll lblPickupPersonName]setText:pasengerNameDisplay];
        [[customCellAll lblPickupTime]setText:[timeDateDF stringFromDate:jobdetail.pickupTime]];
        [[customCellAll lblPickupAddress]setText:jobdetail.puSuburb];
        
        return customCellAll;
    }
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //To be changed: Every section should have row equal to booking available for that particular day
    int count;
    switch (section) {
        case 0:
            count = [HXMainModel sharedInstance].jobDetailsList.count;
            break;
        default:
            break;
    }
    return count;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //To be changed: No of section = No Bookings for different days
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Interaction will be enabled only for dispatched cell hence no need to check again
    
    if ([HXMainModel sharedInstance].dispatchJobDetailsList.count>0)
    {
        if ([HXMainModel sharedInstance].dispatchJobDetailsList.count>indexPath.row)
        {
            jobdetail = [HXMainModel sharedInstance].jobDetailsList[indexPath.row];
            
            HXJobDetails *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"JobDetails"];
            [HXMainModel sharedInstance].jobDetailArrayIndex = indexPath.row;
            [HXMainModel sharedInstance].selectedRefBookingID = jobdetail.bookingID;
            
            [self.navigationController pushViewController:vc animated:YES];
            
        }
    }
    
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    //header string to change: Dynamically
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 18)];
    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 4, tableView.frame.size.width, 18)];
    [label setFont:[UIFont boldSystemFontOfSize:14]];
    [label setTextColor:[UIColor blackColor]];
      NSString *headerStr =@"";
    switch (section) {
            case 0  :
                headerStr = @"Available Job List";
                break;
//            case 1:
//                headerStr = @"Tomorrow";
//                break;
    }
    /* Section header is in 0th index... */
    [label setText:headerStr];
    [view addSubview:label];
    [view setBackgroundColor:[UIColor colorWithRed:166/255.0 green:177/255.0 blue:186/255.0 alpha:1.0]]; 
    return view;
}

#pragma mark - AlertView Delegate Implementation

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSUserDefaults *currentJob = [NSUserDefaults standardUserDefaults];
    
    if (alertView.tag == 100 && buttonIndex == 1) {
        [currentJob setObject:@"na" forKey:@"jobStatus"];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

#pragma mark - Action Implementation
- (IBAction)btnLogoutAction:(id)sender {
    UIAlertView *displayAlert = [[UIAlertView alloc]initWithTitle:@"Logout" message:@"Are you sure" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    [displayAlert setTag:100];
    [displayAlert show];
}

#pragma mark - Method Implementation
-(void)retriveCurrentStatus
{
    NSUserDefaults *currentJob = [NSUserDefaults standardUserDefaults];
    currentStatus = [currentJob stringForKey:@"jobStatus"];
}
@end
