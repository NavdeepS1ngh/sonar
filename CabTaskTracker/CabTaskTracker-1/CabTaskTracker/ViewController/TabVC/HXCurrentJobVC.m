//
//  HXCurrentJobVC.m
//  CabTaskTracker
//
//  Created by Mithun R on 02/07/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import "HXCurrentJobVC.h"

#import "HXTableCellType1.h"
#import "HXJobLegsVC.h"
#import "HXJobExtrasVC.h"
#import "HXLoginView.h"

#import "HXMainModel.h"
#import "HXJobDetailsModel.h"
#import "HXMaintVehicleTypeModel.h"

#define INCREASESIZECONSTANT _vwIndicatorHolder.frame.size.width/3

@interface HXCurrentJobVC ()
{
    NSArray *messageTemplate;
    
    HXJobDetailsModel *jobdetail;
    NSString *pasengerNameDisplay;
    
    NSDateFormatter *timeDateDF;
    
    
}
@property (strong, nonatomic) NSArray *titleList;
@property (strong, nonatomic) NSArray *navigationStatus;

@property (strong, nonatomic) NSString *currentStatus;
@property (strong, nonatomic) HXJobExtrasVC *jobExtraVC;

@end

@implementation HXCurrentJobVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
       
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"%f",INCREASESIZECONSTANT);
    _jobExtraVC = [self.storyboard instantiateViewControllerWithIdentifier:@"JobExtras"];
    
    jobdetail = [HXMainModel sharedInstance].dispatchJobDetailsList[[HXMainModel sharedInstance].jobDetailArrayIndex];
    
    timeDateDF = [[NSDateFormatter alloc] init];
    [timeDateDF setDateFormat:@"HH:mm:ss dd-MMM-yyyy"];
    
    [self createTblData];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self retriveCurrentStatus];
    [self loadUI];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Method Implementation


-(void)createTblData
{
    _titleList = [[NSArray alloc]initWithObjects:@"Job ID",@"PU Time",@"Account",@"Passenger(s)",@"PU Address",@"DR Address",@"Spl Instrns",@"Vehicle Type",@"Legs", nil];
    
    _navigationStatus = [[NSArray alloc]initWithObjects:@"N",@"N",@"N",@"P",@"P",@"P",@"P",@"N",@"Y", nil];
      messageTemplate = [[NSArray alloc]initWithObjects:@" I will reach in ten minutes.",@"I am waiting at the Pickup Point.", nil];
    
    
    if (jobdetail.passengersList.count > 1)
    {
        pasengerNameDisplay = [NSString stringWithFormat:@"%@ & %i More", jobdetail.passengersList[0], jobdetail.passengersList.count];
    }
    else if (jobdetail.passengersList.count == 0)
    {
        pasengerNameDisplay = @"";
    }
    else
    {
        pasengerNameDisplay = jobdetail.passengersList[0];
    }
    
}

-(void)createAlertWithTitle:(NSString *)alertTitle andMessage:(NSString *)alertMessage
{
    UIAlertView *popAlert = [[UIAlertView alloc]initWithTitle:alertTitle message:alertMessage delegate:self cancelButtonTitle:@"Close" otherButtonTitles: nil];
    [popAlert show];
}

#pragma mark - method Implementation

-(void)retriveCurrentStatus
{
    NSUserDefaults *currentJob = [NSUserDefaults standardUserDefaults];
    _currentStatus = [currentJob stringForKey:@"jobStatus"];
}

-(void)loadUI
{
    
    int increaseRatio = 0;
    
    if ([_currentStatus isEqualToString:@"na"]) {
        //Such state does not exist in this view
    }
    else if ([_currentStatus isEqualToString:@"Accepted"])
    {
        [_btnOnSite setUserInteractionEnabled:YES];
        [_btnOnSite setEnabled:YES];
        [_btnOnSite setSelected:NO];
        
        [_btnProgress setUserInteractionEnabled:NO];
        [_btnProgress setEnabled:NO];
        [_btnProgress setSelected:NO];
        
        [_btnCompleted setUserInteractionEnabled:NO];
        [_btnCompleted setEnabled:NO];
        [_btnCompleted setSelected:NO];
        
        increaseRatio = 0;
    }
    else if ([_currentStatus isEqualToString:@"OnSite"])
    {
        [_btnOnSite setUserInteractionEnabled:NO];
        [_btnOnSite setEnabled:YES];
        [_btnOnSite setSelected:YES];
        
        [_btnProgress setUserInteractionEnabled:YES];
        [_btnProgress setEnabled:YES];
        [_btnProgress setSelected:NO];
        
        [_btnCompleted setUserInteractionEnabled:NO];
        [_btnCompleted setEnabled:NO];
        [_btnCompleted setSelected:NO];
        
        increaseRatio = 1;
    }
    else if ([_currentStatus isEqualToString:@"Progress"])
    {
        [_btnOnSite setUserInteractionEnabled:NO];
        [_btnOnSite setEnabled:YES];
        [_btnOnSite setSelected:YES];
        
        [_btnProgress setUserInteractionEnabled:NO];
        [_btnProgress setEnabled:YES];
        [_btnProgress setSelected:YES];
        
        [_btnCompleted setUserInteractionEnabled:YES];
        [_btnCompleted setEnabled:YES];
        [_btnCompleted setSelected:NO];
        
        increaseRatio = 2;
    }
    else if ([_currentStatus isEqualToString:@"Completed"])
    {
        [_btnOnSite setUserInteractionEnabled:NO];
        [_btnOnSite setEnabled:YES];
        [_btnOnSite setSelected:YES];
        
        [_btnProgress setUserInteractionEnabled:NO];
        [_btnProgress setEnabled:YES];
        [_btnProgress setSelected:YES];
        
        [_btnCompleted setUserInteractionEnabled:NO];
        [_btnCompleted setEnabled:YES];
        [_btnCompleted setSelected:YES];
        
        increaseRatio = 3;
    }
    
    [UIView animateWithDuration:0.40 animations:^{
        [_vwIndicatorBar setFrame:CGRectMake(_vwIndicatorBar.frame.origin.x, _vwIndicatorBar.frame.origin.y, INCREASESIZECONSTANT * increaseRatio, _vwIndicatorBar.frame.size.height)];
    }];
}

#pragma mark - Action Implementation

- (IBAction)btnStateChangeAction:(id)sender {
    
    UIButton *currentButton = (UIButton *) sender;
    NSString *displayMessage;
    
    if (currentButton.tag == EONSITE)
    {
        displayMessage = @"Change status from \"Accepted\" to \"On-Site\"?";
    }
    else if (currentButton.tag == EPROGRESS)
    {
        displayMessage = @"Change status from \"On-Site\" to \"Progress\"?";
    }
    else if (currentButton.tag == ECOMPLETED)
    {
        displayMessage = @"Change status from \"Progress\" to \"Completed\"?";
    }
    
    UIAlertView *displayAlert = [[UIAlertView alloc]initWithTitle:@"Are you sure?" message:displayMessage delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    [displayAlert show];
    
}

- (IBAction)btnLogoutAction:(id)sender {
    UIAlertView *displayAlert = [[UIAlertView alloc]initWithTitle:@"Logout" message:@"Are you sure" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    [displayAlert setTag:100];
    [displayAlert show];
    
}

- (IBAction)btnMessage:(id)sender {
  
            [viewMessage setFrame:CGRectMake(viewMessage.frame.origin.x, 1000, viewMessage.frame.size.width, viewMessage.frame.size.height)];
            [viewTransparent setFrame:CGRectMake(viewTransparent.frame.origin.x, 1000, viewTransparent.frame.size.width, viewTransparent.frame.size.height)];
    
            [tblMessageTemplate setFrame:CGRectMake(tblMessageTemplate.frame.origin.x,64, tblMessageTemplate.frame.size.width, tblMessageTemplate.frame.size.height)];
                         
                         
            navigationBar.topItem.title = @"Select Message";
                
  
}
#pragma mark -  MFMessageComposeViewControllerDelegate
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
	switch (result) {
		case MessageComposeResultCancelled:
			NSLog(@"Cancelled");
			break;
		case MessageComposeResultFailed:{
            UIAlertView *Notpermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [Notpermitted show];
        }
			break;
		case MessageComposeResultSent:
            
			break;
		default:
			break;
	}
    
	[self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - AlertView Delegate Implementation

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSUserDefaults *currentJob = [NSUserDefaults standardUserDefaults];
    
    if (buttonIndex == 1) {
        
        if (alertView.tag == 100) {
            [currentJob setObject:@"na" forKey:@"jobStatus"];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        else
        {
            if ([_currentStatus isEqualToString:@"Accepted"]) {
                [currentJob setObject:@"OnSite" forKey:@"jobStatus"];
            }
            else if ([_currentStatus isEqualToString:@"OnSite"]) {
                [currentJob setObject:@"Progress" forKey:@"jobStatus"];
            }
            else if ([_currentStatus isEqualToString:@"Progress"]) {
                [self.navigationController pushViewController:_jobExtraVC animated:YES];
            }
            else if ([_currentStatus isEqualToString:@"Completed"]) {
                
            }
            [self retriveCurrentStatus];
            [self loadUI];
        }
    }
}

#pragma mark - TableView Delegate and DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int count = 0;
    if (tableView == _tblCurerntJob)
        count =  (int)_titleList.count;
    else if (tableView == tblMessageTemplate)
        count =  (int) messageTemplate.count;
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cellMessage;
    HXTableCellType1 *cell;
    
    if (tableView == _tblCurerntJob)
    {
        static NSString *CellIdentifier = @"cellType1";
        
        cell = (HXTableCellType1 *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        cell.lblTitle.text = _titleList[indexPath.row];
        
        if ([_navigationStatus[indexPath.row] isEqualToString:@"P"]) {
            [cell.imgDetails setHidden:NO];
            [cell.imgDetails setHighlighted:YES];
        }
        else if ([_navigationStatus[indexPath.row] isEqualToString:@"Y"])
        {
            [cell.imgDetails setHidden:NO];
            [cell.imgDetails setHighlighted:NO];
        }
        else
        {
            [cell.imgDetails setHidden:YES];
        }
        
        switch (indexPath.row) {
            case EJOBID:
                cell.lblContent.text = jobdetail.bookingNumber;
                break;
            case EPUTIME:
                cell.lblContent.text = [timeDateDF stringFromDate:jobdetail.pickupTime];
                break;
            case EACCOUNT:
                cell.lblContent.text = jobdetail.accountName;
                break;
            case EPASSENGER:
                cell.lblContent.text = pasengerNameDisplay;
                break;
            case EPUADDRESS:
                cell.lblContent.text = jobdetail.puSuburb;
                break;
            case EDRADDRESS:
                cell.lblContent.text = jobdetail.drAddress;
                break;
            case ESPLINSTRUCTION:
                cell.lblContent.text = jobdetail.specialInstructions;
                break;
            case EVEHICLETYPE:
                if (!([jobdetail.refVehicleType isEqualToString:@""] || jobdetail.refVehicleType == (id)[NSNull null] || jobdetail.refVehicleType == nil)) {
                    for (HXMaintVehicleTypeModel *vehicleDetail in [HXMainModel sharedInstance].allVehicleList) {
                        if([vehicleDetail.utilID isEqualToString:jobdetail.refVehicleType])
                        {
                            cell.lblContent.text = vehicleDetail.utilDescription;
                            break;
                        }
                    }
                }
                break;
            case ELEGS:
                cell.lblContent.text = [NSString stringWithFormat:@"%i",[HXMainModel sharedInstance].additionalJobDetailsModel.legList.count];
                break;
        }

        return cell;
    }
    
    else if (tableView == tblMessageTemplate)
    {
        cellMessage = [tableView dequeueReusableCellWithIdentifier:@"MessageCell"];
        
        if (cellMessage == nil) {
            cellMessage = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MessageCell"];
        }

        cellMessage.textLabel.text = [messageTemplate objectAtIndex:indexPath.row];
        return cellMessage;
    }

    return cellMessage;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _tblCurerntJob)
    {
        switch (indexPath.row) {
            case EPASSENGER:
            {
                NSMutableString *passengerListString = [[NSMutableString alloc]init];
                if (jobdetail.passengersList.count == 0) {
                    [passengerListString appendString:@""];
                }
                for (NSString *passenger in jobdetail.passengersList) {
                    [passengerListString appendString:[NSString stringWithFormat:@"%@\n", passenger]];
                }
                [self createAlertWithTitle:@"Passenger List" andMessage:passengerListString];
                //For inbuilt message for passengers:
                /*
                [UIView animateWithDuration:0.3
                                      delay:0.3
                                    options: UIViewAnimationOptionCurveEaseInOut
                                 animations:^
                                    {
                                     if ([[UIDevice currentDevice].model isEqualToString:@"iPad"])
                                     {
                                         [viewMessage setFrame:CGRectMake(viewMessage.frame.origin.x, 376, viewMessage.frame.size.width, viewMessage.frame.size.height)];
                                         [viewTransparent setFrame:CGRectMake(viewTransparent.frame.origin.x, 0, viewTransparent.frame.size.width, viewTransparent.frame.size.height)];
                                     }
                                     else
                                     {
                                     [viewMessage setFrame:CGRectMake(viewMessage.frame.origin.x, 162, viewMessage.frame.size.width, viewMessage.frame.size.height)];
                                     [viewTransparent setFrame:CGRectMake(viewTransparent.frame.origin.x, 0, viewTransparent.frame.size.width, viewTransparent.frame.size.height)];
                                     }
                                 }
                                 completion:^(BOOL finished){
                                 
                                 }];
                [self.view bringSubviewToFront:viewTransparent];
                [self.view bringSubviewToFront:viewMessage];
                 */
            }
            break;
            
            case EPUADDRESS:
                [self createAlertWithTitle:@"Pick-Up Address" andMessage:jobdetail.puAddress];
                break;
                
            case EDRADDRESS:
                [self createAlertWithTitle:@"Drop Address" andMessage:jobdetail.drAddress];
                break;
            case ESPLINSTRUCTION:
                [self createAlertWithTitle:@"Special Instructions" andMessage:jobdetail.specialInstructions];
                break;
                
            case ELEGS:
            {
                HXJobLegsVC *legsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"JobLegs"];
                [self.navigationController pushViewController:legsVC animated:YES];
            }
                break;
                
                
            default:
            break;
        }
    }
    else if (tableView == tblMessageTemplate)
    {
        MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
        if([MFMessageComposeViewController canSendText])
        {
            controller.body =[NSString stringWithFormat:@"%@",[messageTemplate objectAtIndex:indexPath.row]];
            controller.recipients = [NSArray arrayWithObjects:@"+919468671444", nil];
            controller.messageComposeDelegate = (id)self;
            //	[self presentModalViewController:controller animated:YES];
            [self presentViewController:controller animated:YES completion:nil];
            
        }
   
        [tblMessageTemplate setFrame:CGRectMake( tblMessageTemplate.frame.origin.x, 1000,tblMessageTemplate.frame.size.width, tblMessageTemplate.frame.size.height)];
          navigationBar.topItem.title = @"Current Job";
    }
    
}
#pragma  mark - touch events
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [viewMessage setFrame:CGRectMake(viewMessage.frame.origin.x, 1000, viewMessage.frame.size.width, viewMessage.frame.size.height)];
    [viewTransparent setFrame:CGRectMake(viewTransparent.frame.origin.x, 1000, viewTransparent.frame.size.width, viewTransparent.frame.size.height)];
    
}
@end
