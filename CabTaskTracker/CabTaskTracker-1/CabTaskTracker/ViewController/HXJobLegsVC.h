//
//  HXJobLegsVC.h
//
//  Created by Mithun R on 03/07/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HXJobLegsVC : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property(strong,nonatomic)IBOutlet  UITableView *tblJobsDetails;
@property(strong,nonatomic) NSMutableArray *legsArray;



- (IBAction)btnBackAction:(id)sender;


@end
