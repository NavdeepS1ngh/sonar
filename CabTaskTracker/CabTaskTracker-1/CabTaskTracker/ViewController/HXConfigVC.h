//
//  HXConfigVC.h
//  CabTaskTracker
//
//  Created by Vignesh Kumar on 18/07/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import <UIKit/UIKit.h>

//#import "HXServiceMgrProtocol.h"
#import "HXModelMgrProtocol.h"

@interface HXConfigVC : UIViewController <HXModelMgrProtocol>

@property (weak, nonatomic) IBOutlet UITextField *tbChfID;
@property (weak, nonatomic) IBOutlet UITextField *tbPassword;


- (IBAction)btnBackAction:(id)sender;
- (IBAction)btnConfigureAction:(id)sender;
@end
