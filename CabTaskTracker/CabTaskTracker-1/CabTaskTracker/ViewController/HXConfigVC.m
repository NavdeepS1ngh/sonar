//
//  HXConfigVC.m
//  CabTaskTracker
//
//  Created by Vignesh Kumar on 18/07/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import "HXConfigVC.h"

#import "HXMainModel.h"
#import "HXCoreDataManager.h"

@interface HXConfigVC ()
{
    HXAuthenticationModel *authenticationModel;
    
    NSManagedObjectContext *context;
    NSEntityDescription *entityDesc;
    HXCoreDataManager* coreDataManager;
    
}
@end

@implementation HXConfigVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark -  Methods Implementation

//For KeyBoard Notification.
-(void) registerForKeyboardNotifications
{
    // register for keyboard notifications
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:self.view.window];
	// register for keyboard notifications
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:)
												 name:UIKeyboardWillHideNotification object:self.view.window];
}


#pragma mark - Keyboard Delegates, Notification and Method

- (void)keyboardWillShow:(NSNotification *)notif{
    
//
    
}

- (void)keyboardWillHide:(NSNotification *)notif{
    
    //
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

#pragma mark - Action Implementation

- (IBAction)btnBackAction:(id)sender {
    [[self navigationController]popToRootViewControllerAnimated:YES];
}

- (IBAction)btnConfigureAction:(id)sender {
    
    if([self textBoxValidation])
    {
        if ([HXMainModel sharedInstance].isNetworkConnected) {

            authenticationModel = [[HXAuthenticationModel alloc]init];
            [authenticationModel setModelDelegate:self];
            [authenticationModel configureUserWithUserName:_tbChfID.text andPassword:[HXMainModel getEncryptedPasswordFor:_tbPassword.text]];
            
        }
        else
        {
            UIAlertView *popAlert = [[UIAlertView alloc]initWithTitle:@"Configuration Error" message:@"No Network! \n Please try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            
            [popAlert show];
        }
    }

}

#pragma mark - Action Implementation
-(BOOL)textBoxValidation
{
    //Add any validation
    BOOL isValid = ![_tbChfID.text isEqualToString:@""] && ![_tbPassword.text isEqualToString:@""];
    return isValid;
}

#pragma mark - Method Implementation


#pragma mark - Protocol Implementation
-(void)saveCredentialInDB
{
    coreDataManager = [HXCoreDataManager sharedInstance];
    context = [coreDataManager managedObjectContext];
    entityDesc =    [NSEntityDescription entityForName:@"Chauffeur"
                                inManagedObjectContext:context];
    NSError* fetchError;
    NSFetchRequest* fetchReq = [[NSFetchRequest alloc]init];
    [fetchReq setEntity:entityDesc];
    
    NSPredicate* fetchPredicate = [NSPredicate predicateWithFormat:@"chauffeurID==%@",_tbChfID.text];
    [fetchReq setPredicate:fetchPredicate];
     NSManagedObject* tblRow;
     NSError *error;
    NSArray* results = [context executeFetchRequest:fetchReq error:&fetchError];
    if (fetchError) {
        NSLog(@"Error : %@",fetchError);
    }
    else
    {// no error! SUCCESSFULLY FETCHED !
        if (results.count>0) {
            for (NSManagedObject* object in results) {
                //need to chk 
                [object setValue:[HXMainModel getEncryptedPasswordFor:_tbPassword.text]forKey:@"password"];
                [context save:&error];
                
                break;
            }
            [context save:&fetchError];
            if (error) {
                NSLog(@"Record Not updated");
                
                UIAlertView *popAlert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Error in configuring Chaffer.!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [popAlert show];
            }
            else
            {
                NSLog(@"Record  updated");
                
                UIAlertView *popAlert = [[UIAlertView alloc]initWithTitle:@"Success" message:@"Chauffeur updated sucesfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [popAlert show];
            }
        }
        else
        {
            entityDesc =    [NSEntityDescription entityForName:@"Chauffeur"
                                        inManagedObjectContext:context];
            tblRow = [NSEntityDescription insertNewObjectForEntityForName:@"Chauffeur" inManagedObjectContext:context];
            [tblRow setValue:_tbChfID.text forKey:@"chauffeurID"];
            [tblRow setValue:@"12-07-07" forKey:@"licenseExpiryDate"];
            [tblRow setValue:[HXMainModel getEncryptedPasswordFor:_tbPassword.text] forKey:@"password"];
            [tblRow setValue:@"12-09-09" forKey:@"updateDate"];
            
           
            [context save:&error];
            if (error) {
                NSLog(@"Record Not inserted");
                UIAlertView *popAlert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Error in configuring Chauffeur.!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [popAlert show];
            }
            else
            {
                NSLog(@"Record  inserted");
                UIAlertView *popAlert = [[UIAlertView alloc]initWithTitle:@"Success" message:@"Chauffeur configured sucesfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [popAlert show];
            }
            
        }
    }

}
-(void)authenticationErrorDispaly
{
    UIAlertView *popAlert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Error in configuring Chauffeur" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [popAlert show];
}


@end
