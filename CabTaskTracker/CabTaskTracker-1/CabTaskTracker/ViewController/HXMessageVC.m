//
//  HXMessageVC.m
//  CabTaskTracker
//
//  Created by aditi on 03/07/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import "HXMessageVC.h"
#import "HXMessageCell.h"
@interface HXMessageVC ()

@end

@implementation HXMessageVC
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Table View delegate


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    HXMessageCell *cell = (HXMessageCell *)[tableView dequeueReusableCellWithIdentifier:@"MessageCell"];
   
    cell.lblMessageSubject.text=@"Arrive at ... ";
    cell.lblMessageRecievedTime.text=@"09:30";
    
    return cell;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
     [tableView setFrame:CGRectMake(tableView.frame.origin.x, tableView.frame.origin.y, tableView.frame.size.width, 44*1)];
    // no of cell x cell height
    return 1;
   
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self createAlertWithTitle:@"Arrive at the gate 10 minutes pror to pickup" :@"Please see"];
}
- (IBAction)btnBackAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)createAlertWithTitle : (NSString*) alertTitle :(NSString*) alertMessage
{
    UIAlertView* popAlert = [[ UIAlertView alloc ]initWithTitle:alertTitle message:alertMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [popAlert show]; 
}
@end
