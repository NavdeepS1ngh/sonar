//
//  HXMessageCell.h
//  CabTaskTracker
//
//  Created by aditi on 03/07/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HXMessageCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblMessageSubject;
@property (strong, nonatomic) IBOutlet UILabel *lblMessageRecievedTime;

@end
