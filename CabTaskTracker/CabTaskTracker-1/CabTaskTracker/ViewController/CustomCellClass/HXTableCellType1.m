//
//  HXTableCellType1.m
//  CabTaskTracker
//
//  Created by Mithun R on 02/07/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import "HXTableCellType1.h"

@implementation HXTableCellType1

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
