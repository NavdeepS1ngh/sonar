//
//  HXLoginView.m
//  WireFrames
//
//  Created by Mithun R on 26/06/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import "HXLoginView.h"
#import "HXJobListVC.h"
#import "HXTabViewController.h"

#import "HXWebServiceManager.h"

#import "HXCoreDataManager.h"
#import "HXMainModel.h"

/* center position value*/
#define CENTER_Y_VAL_UP 150  //90
#define CENTER_Y_VAL_DOWN 250   //170
#define VHCLISTINTIALTITLE @"Select Vehicle from list"

#define is4inchScreen  ([[UIScreen mainScreen] bounds].size.height == 568)?TRUE:FALSE

@interface HXLoginView ()
{
    
    HXAuthenticationModel *authenticationModel;
    
    NSManagedObjectContext *context;
    NSEntityDescription *entityDesc;
    HXCoreDataManager* coreDataManager;
    UIButton* activeDropDownBtn;
    
    NSMutableArray* chaueffuerIDArray;
    NSMutableArray* vehicleIdArray;
    
    BOOL isChauffeurConfigured;
    NSString* pwdFromLocal;

}
@end

@implementation HXLoginView
@synthesize  imgLogo,btnLogin,txtPassword,subView,btnVehichleId;
BOOL isSingleReply;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    authenticationModel = [[HXAuthenticationModel alloc]init];
    
    vehicleIdArray = [[NSMutableArray alloc]init];
    chaueffuerIDArray = [[NSMutableArray alloc]init];
    
    self.title = @"Login";
    self.view.backgroundColor = [UIColor colorWithRed:0/255.0 green:129/255.0 blue:95/255.0 alpha:1];
    self.subView.backgroundColor = [UIColor colorWithRed:0/255.0 green:129/255.0 blue:95/255.0 alpha:1];
    self.imgLogo.backgroundColor = [UIColor darkGrayColor];
    self.imgLogo.layer.cornerRadius =  10.0f;
    self.imgLogo.clipsToBounds = YES;
    self.btnLogin = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
//    [btnVehichleId.titleLabel setText:VHCLISTINTIALTITLE];
    
    [self.navigationController setNavigationBarHidden:YES];
    
    //For KeyBoard Notification.
    [self registerForKeyboardNotifications];
    
    //Date Test Purpose:
    /*
     NSString * dateString = @"2014-08-01T16:00:31.3274374";
     NSDateFormatter * myDateFormatter = [[NSDateFormatter alloc] init];
     [myDateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
     NSDate * dateFromString = [myDateFormatter dateFromString:dateString];
     NSLog(@"Print Date: %@", dateFromString);
     //*/
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
 //loading vehicleIdArray
   // vehicleIdArray = [[HXMainModel sharedInstance]getVehicleIDFromDB];
    
    [self loadDropDownLists];
    
    [btnLogin setEnabled:NO];

}
- (void)viewWillDisappear:(BOOL)animated{
    // unregister for keyboard notifications while not visible.
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    
    dropdown = nil;
    [dropdown hideDropDown:activeDropDownBtn];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -  Methods Implementation

-(void) navigateToNextVC
{
    HXTabViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"TabVC"];
    [self.navigationController pushViewController:vc animated:YES];
}

//For KeyBoard Notification.
-(void) registerForKeyboardNotifications
{
    // register for keyboard notifications
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:self.view.window];
	// register for keyboard notifications
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:)
												 name:UIKeyboardWillHideNotification object:self.view.window];
}

-(void)loginValidation
{
    if ([HXMainModel sharedInstance].isNetworkConnected) {//connection avaiable
        
        [authenticationModel setModelDelegate:self];
        [authenticationModel configureUserWithUserName:_tbChauffurID.text andPassword:[HXMainModel getEncryptedPasswordFor:m_password]];
    }
    else // connection not available
    {
        if (isChauffeurConfigured) {
            if ([pwdFromLocal isEqualToString:txtPassword.text]) {//comparing with the password stored in local
                //need to check syncmaintenece here
                if ([[HXMainModel sharedInstance] isMaintenanceDataAvailable]) {
                    [[HXMainModel sharedInstance] setIsOfflineLogin:YES];
                    
                    //Load the available booking before navigating:
                    [self loadJobListModel];
                    [self navigateToNextVC];//moving to next screen in offline mode
                }
                else
                {//no maintenence data available
                    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"No Maintenance Data Available!" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alert show];
                }
            }
            else
            {//incorrect password
                UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Please enter valid password" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alert show];
                
            }
        }
        else
        {//user does not exists in CHauffeur entity
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Chauffeur is not configured. Need connection to configure the chauffeur" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
        }
    }
}

-(void) loadDropDownLists
{
    //loading chauffeuridArray
    NSArray* objects = [[HXCoreDataManager sharedInstance]getMatchedRowsFromEntity:@"Chauffeur" predicateString:@""];
    
    for (int i = 0; i<objects.count; i++) {
        NSManagedObject *obj = [objects objectAtIndex:i];
        [chaueffuerIDArray addObject:[obj valueForKey:@"chauffeurID"]];
    }
    
    [context save:nil];
    
    
    //////loading vehicleIdArray////////
    
    objects = [[HXCoreDataManager sharedInstance]getMatchedRowsFromEntity:@"Vehicle" predicateString:@""];
    [vehicleIdArray removeAllObjects];
    for (int i = 0; i<objects.count; i++) {
        NSManagedObject *obj = [objects objectAtIndex:i];
        [vehicleIdArray addObject:[NSString stringWithFormat:@"%@",[obj valueForKey:@"vehicleID"]]];
    }
    
    [context save:nil];
}

-(void)loadJobListModel
{
    [[HXMainModel sharedInstance]setControllerDelegate:self];
    [[HXMainModel sharedInstance]fetchDataForJobDetails];
    
}


#pragma mark - Action Implementation

- (IBAction)btnRefreshAction:(id)sender {
    
    if ([HXMainModel sharedInstance].isNetworkConnected)
    {
        [_loadingIndicator startAnimating];
        [_mainView setUserInteractionEnabled:NO];
        
        //chechk if the chauffeur is configured
        
        if (isChauffeurConfigured) {
            [[HXMainModel sharedInstance]setControllerDelegate:self];
            [[HXMainModel sharedInstance]getVehicleList];
        }
        else
        {
            [[HXMainModel sharedInstance]setIsVehicleListCallPending:YES];
            [authenticationModel setModelDelegate:self];
            [authenticationModel configureUserWithUserName:_tbChauffurID.text andPassword:[HXMainModel getEncryptedPasswordFor:txtPassword.text]];
        }
    }
    else
    {
        UIAlertView *popAlert = [[UIAlertView alloc]initWithTitle:@"Configuration Error" message:@"No Network! \n Please try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        
        [popAlert show];
    }
}

-(IBAction)TextField_Exit:(id) sender
{
    [sender resignFirstResponder];
}

- (IBAction)btnOpenDropdownAction:(id)sender {
    activeDropDownBtn = sender;
    NSArray* tempArr = [[NSArray alloc]init];
    if (activeDropDownBtn == btnVehichleId)
        tempArr = vehicleIdArray;
    
    if (tempArr.count == 0 || tempArr == nil) {
        return;
    }
    if(dropdown == nil) {
        CGFloat f = 200;
        
        dropdown = [[NIDropDown alloc] showDropDown:sender withHeight:&f withArray:tempArr withImgArray:nil andDirection:@"down"];
        dropdown.delegate = self;
        
        [subView.superview bringSubviewToFront:subView];
        [self.view bringSubviewToFront:subView.superview];
    }
    else {
        [dropdown hideDropDown:sender];
        dropdown = nil;
    }
}

-(IBAction)btnLoginAction:(id)sender{
    
    if(self.txtPassword.text.length && self.btnVehichleId.titleLabel.text.length){
        
        m_password = [self.txtPassword.text copy];
        m_vehicleid = [self.btnVehichleId.titleLabel.text copy];
        
        //netwrok indicator
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        
        [_loadingIndicator startAnimating];

        [self loginValidation];
        
    }
    
    else if([self.txtPassword.text isEqualToString:@""] && [self.btnVehichleId.titleLabel.text isEqualToString:@""]){
        
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Please enter UserName,Password & Vehicleid" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        
    }
    
    else if([self.txtPassword.text isEqualToString:@""] && [self.btnVehichleId.titleLabel.text isEqualToString:@""]){
        
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Please enter Password & Vehicleid" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        
    }
    
    else if([self.btnVehichleId.titleLabel.text isEqualToString:@""]){
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Please enter VehicleID" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        
    }
}

#pragma mark - ControllerMgrProtocol Implementation
-(void)vehicleListDisplayWithStatus:(NSString *)message
{
    if ([message isEqualToString:@"OK"]) {
        [self loadDropDownLists];
        [[HXMainModel sharedInstance]setIsVehicleListCallPending:NO];
        
    }
    else if ([message isEqualToString:@"UNAUTHORIZED"])
    {
        //UserName and Psw Error;
        
        [[HXMainModel sharedInstance]setIsVehicleListCallPending:YES];
        
        [authenticationModel setModelDelegate:self];
        [authenticationModel configureUserWithUserName:_tbChauffurID.text andPassword:[HXMainModel getEncryptedPasswordFor:txtPassword.text]];
        
    }
    else if ([message isEqualToString:@"FOUND"])
    {
        //Display alert
    }
    else if ([message isEqualToString:@"INTERNALSERVERERROR"])
    {
        //Display alert
    }
    else if ([message isEqualToString:@"ENOTFOUNT"])
    {
        //Display alert
    }
    else if ([message isEqualToString:@"BADREQUEST"])
    {
        //Display alert
    }
    
    [_loadingIndicator stopAnimating];
    [_mainView setUserInteractionEnabled:YES];
}

//MOdel Protocol
-(void)saveCredentialInDB
{
    context = [[HXCoreDataManager sharedInstance]managedObjectContext];
    NSLog(@"%@",[[HXMainModel sharedInstance] currentUser]);
    
    
    NSString *predicateString = [NSString stringWithFormat:@"chauffeurID==\"%@\"", [[HXMainModel sharedInstance] currentUser]];
    
    
    NSArray* results = [[HXCoreDataManager sharedInstance]getMatchedRowsFromEntity:@"Chauffeur" predicateString:predicateString];
    
    NSError* error;
    if (results.count > 0) {
        NSManagedObject*  tblRow = [results objectAtIndex:0];
        [tblRow setValue:_tbChauffurID.text forKey:@"chauffeurID"];
        [tblRow setValue:@"12-07-07" forKey:@"licenseExpiryDate"];
        [tblRow setValue:[HXMainModel getEncryptedPasswordFor:txtPassword.text] forKey:@"password"];
        
        
        [context save:&error];
        
    }
    else
    {
        
        NSManagedObject* tblRow = [NSEntityDescription insertNewObjectForEntityForName:@"Chauffeur" inManagedObjectContext:context];
        [tblRow setValue:_tbChauffurID.text forKey:@"chauffeurID"];
        [tblRow setValue:@"12-07-07" forKey:@"licenseExpiryDate"];
        [tblRow setValue:[HXMainModel getEncryptedPasswordFor:m_password] forKey:@"password"];
        
        
        [context save:&error];
        
        
        if (error) {
            NSLog(@"Record Not inserted");
            UIAlertView *popAlert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Error in configuring Chauffeur.!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [popAlert show];
        }
        else
        {
            NSLog(@"Record  inserted");
            UIAlertView *popAlert = [[UIAlertView alloc]initWithTitle:@"Success" message:@"Chauffeur configured sucesfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [popAlert show];
        }
    }
    
    //netwrok indicator
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    [_loadingIndicator stopAnimating];

    isChauffeurConfigured = YES;
    //once the credentials updated/inserted in Chauffeur Entity successfully
    
    if ([[HXMainModel sharedInstance]isVehicleListCallPending]) {
        [self btnRefreshAction:nil];
    }
    else{
        //Temp change for Demo
       // [self navigateToNextVC];
        [[HXMainModel sharedInstance]setControllerDelegate:self];
        [[HXMainModel sharedInstance]getMaintenanceData];
    }
}

//TODO: call maintenance api
//using model calss and its request, and set contotller protocol

//Controller Protocol
-(void)requiredDataFetch_Result:(NSString *)message
{
    if ([message isEqualToString:@"OK"]) {
        //Navigate to next VC
        //NSMutableArray* jobdetaailsarray = [[HXMainModel sharedInstance]jobDetailsModel].jobDetailsList;
        [self navigateToNextVC];
    }
    else
    {
        //Respective Alert
    }
}

//////
-(void)authenticationErrorDispaly
{
    [_loadingIndicator stopAnimating];
    UIAlertView *popAlert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Error in configuring Chauffeur" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [popAlert show];
}

#pragma mark - Keyboard Delegates, Notification and Method

- (void)keyboardWillShow:(NSNotification *)notif{
    
    //[self setViewMovedUp:YES];
    
}

- (void)keyboardWillHide:(NSNotification *)notif{
    
    //[self setViewMovedUp:NO];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

#pragma mark - NIDropDownDelegate

- (void) niDropDownDelegateMethod: (NIDropDown *) sender  selectedRowId:(NSString *)selectedRowValue {
    if(activeDropDownBtn == btnVehichleId)
    {
        [_btnVhRefresh setEnabled:YES];
        [btnVehichleId setTitle:[NSString stringWithFormat:@"%@",selectedRowValue] forState:UIControlStateNormal];
        
        //enable login button as vehicle has been selected
        [btnLogin setEnabled:YES];
    }
    [dropdown hideDropDown:activeDropDownBtn];
    
    dropdown = nil;
}

#pragma  mark - textfield delegate
- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField == _tbChauffurID) {
        
        NSString *predicateString = [NSString stringWithFormat:@"chauffeurID==\"%@\"", _tbChauffurID.text];
        
        
        NSArray* results = [[HXCoreDataManager sharedInstance]getMatchedRowsFromEntity:@"Chauffeur" predicateString:predicateString];
        
        
        
        if (results.count>0) {
            isChauffeurConfigured = true;
            
            for (NSManagedObject* object in results) {
                pwdFromLocal =  [object valueForKey:@"password"];//storing the chauffeur's password to be used in loginValidation
                break;
            }
            
        }
        else
        {
            isChauffeurConfigured = false;
        }
        
        
        [context save:nil];
        NSLog(@"isChauffeurConfigured : %hhd",isChauffeurConfigured);
        
        [[HXMainModel sharedInstance]setCurrentUser:_tbChauffurID.text];
        
        
    }
}


@end
