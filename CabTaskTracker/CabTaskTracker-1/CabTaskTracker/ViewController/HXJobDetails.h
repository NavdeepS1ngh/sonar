//
//  HXJobDetails.h
//  ETG_App
//
//  Created by aditi on 02/07/14.
//  Copyright (c) 2014 hexaware. All rights reserved.
//

#import <UIKit/UIKit.h>

enum tableOrderList
{
    EJOBID = 0,
    EPUTIME,
    EACCOUNT,
    EPASSENGER,
    EPUADDRESS,
    EDRADDRESS,
    ESPLINSTRUCTION,
    EVEHICLETYPE,
    ELEGS
};

@interface HXJobDetails : UIViewController
{
    IBOutlet UITableView *tblAdditionalReq;
    IBOutlet UITableView *tblJobDetails;
    IBOutlet UIView *viewAdditionalReq;
}
@property (strong, nonatomic) NSArray *titleList;
@property (strong, nonatomic) NSArray *navigationStatus;
@property (strong,nonatomic) NSArray* additionalRequests;

@property (strong, nonatomic) IBOutlet UINavigationItem *navBarItem;
@property (strong, nonatomic) IBOutlet UIButton *btnAccept;


-(void)createTblData;
-(void)createAlertWithTitle:(NSString *)alertTitle andMessage:(NSString *)alertMessage;

- (IBAction)btnBackAction:(id)sender;
- (IBAction)btnAcceptAction:(id)sender;

@end
