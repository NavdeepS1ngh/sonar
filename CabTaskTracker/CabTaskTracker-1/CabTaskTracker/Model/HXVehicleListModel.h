//
//  HXVehicleListModel.h
//  CabTaskTracker
//
//  Created by Mithun on 30/07/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "HXModelMgrProtocol.h"
#import "HXServiceMgrProtocol.h"


@interface HXVehicleListModel : NSObject <HXServiceMgrProtocol>

-(void)getVehicleList;

@property(readwrite,assign)id<HXModelMgrProtocol>modelDelegate;

//Model Data
@property (nonatomic, strong) NSString *vehicleID;
@property (nonatomic, strong) NSString *chauffeurID;
@property (nonatomic, strong) NSString *rego;
@property (nonatomic, strong) NSString *refVehicleType;
@property (nonatomic, strong) NSString * isVehicleActive;


//UtilData
@property (nonatomic, strong) NSString *messageFromServer;
@property (nonatomic, strong) NSString *lastUpdateTime;

//Array of self:
@property (nonatomic, strong) NSMutableArray *vehicleListAry; // To store recieved details

@property (nonatomic, strong) HXVehicleListModel *vehicleModel;
@end
