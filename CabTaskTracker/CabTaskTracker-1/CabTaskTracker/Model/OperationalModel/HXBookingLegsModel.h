//
//  HXBookingLegs.h
//  CabTaskTracker
//
//  Created by Mithun on 06/08/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HXBookingLegsModel : NSObject

@property (nonatomic, strong) NSString *legsID;
@property (nonatomic, strong) NSString *refBooking;
@property (nonatomic, strong) NSString *legNumber;
@property (nonatomic, strong) NSString *legAddress;
@property (nonatomic, strong) NSString *legSuburb;
@property (nonatomic, strong) NSString *legPhone;
@property (nonatomic, strong) NSString *flightNumber;
@property (nonatomic, strong) NSString *isActive;

@end
