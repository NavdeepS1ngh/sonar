//
//  HXBookingModel.h
//  CabTaskTracker
//
//  Created by Mithun on 06/08/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HXBookingModel : NSObject

@property (nonatomic, strong) NSString *bookingID;
@property (nonatomic, strong) NSString *refChauffeur;
@property (nonatomic, strong) NSString *refVehicleType;
@property (nonatomic, strong) NSString *bookingNumber;
@property (nonatomic, strong) NSString *bookingDate;
@property (nonatomic, strong) NSString *pickupTime;
@property (nonatomic, strong) NSString *accountNumber;
@property (nonatomic, strong) NSString *accountName;
@property (nonatomic, strong) NSString *puSuburb;
@property (nonatomic, strong) NSString *puAddress;
@property (nonatomic, strong) NSString *drSuburb;
@property (nonatomic, strong) NSString *drAddress;
@property (nonatomic, strong) NSString *specialInstructions;
@property (nonatomic, strong) NSString *isDispatched;
@property (nonatomic, strong) NSString *allowSundries;
@property (nonatomic, strong) NSNumber *allowKMS;
@property (nonatomic, strong) NSNumber *totalKMS;
@property (nonatomic, strong) NSString *waitingTime;

@end
