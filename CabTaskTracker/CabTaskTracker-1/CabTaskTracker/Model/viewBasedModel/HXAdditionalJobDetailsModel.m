//
//  HXAdditionalJobDetailsModel.m
//  CabTaskTracker
//
//  Created by Mithun on 11/08/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import "HXAdditionalJobDetailsModel.h"
#import "HXCoreDataManager.h"
#import "HXBookingLegsModel.h"

@implementation HXAdditionalJobDetailsModel
{
    HXBookingLegsModel *legDetails;
}


-(void)loadAdditionalData
{
    [self getAdditionalRequestList];
    [self getLegList];
}

-(void)getAdditionalRequestList
{
    _additionalRequestList = [[NSMutableArray alloc]init];
    
    //FOr reffered bookingID in string
    NSString* predicateString = [NSString stringWithFormat:@"refBooking==%@",_refBookingID];
    NSArray* results = [[HXCoreDataManager sharedInstance] getMatchedRowsFromEntity:@"Booking_Request" predicateString:predicateString];
    
    for (NSManagedObject* obj in results) {
  
        [_additionalRequestList addObject:[obj valueForKey:@"requestID"]];
    }
    
    [[HXCoreDataManager sharedInstance] saveContext];
    
}

-(void)getLegList
{
    _legList = [[NSMutableArray alloc]init];
    
    //FOr reffered bookingID in string

    NSString* predicateString = [NSString stringWithFormat:@"refBooking==%@",_refBookingID];
    
    NSArray* results = [[HXCoreDataManager sharedInstance] getMatchedRowsFromEntity:@"Booking_Legs" predicateString:predicateString];
    for (NSManagedObject* obj in results) {
        legDetails = [[HXBookingLegsModel alloc]init];

        legDetails.legsID = [obj valueForKey:@"legsID"];
        legDetails.legSuburb = [obj valueForKey:@"legSuburb"];
        legDetails.legPhone = [obj valueForKey:@"legPhone"];
        legDetails.legNumber = [obj valueForKey:@"legNumber"];
        legDetails.legAddress = [obj valueForKey:@"legAddress"];
        legDetails.isActive = [obj valueForKey:@"isActive"];
        legDetails.flightNumber = [obj valueForKey:@"flightNumber"];
        legDetails.refBooking = [obj valueForKey:@"refBooking"];

        
        
        [_legList addObject:legDetails];
        
    }
    [[HXCoreDataManager sharedInstance]saveContext];
}

@end
