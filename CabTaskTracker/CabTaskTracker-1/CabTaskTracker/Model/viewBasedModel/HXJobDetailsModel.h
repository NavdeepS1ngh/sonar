//
//  HXJobDetailsModel.h
//  CabTaskTracker
//
//  Created by Mithun on 07/08/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HXJobDetailsModel : NSObject

//Operational Method//

-(void)loadJobDetailsFromDB;


////Primary Display Data////

@property (nonatomic, strong) NSString *bookingID;
@property (nonatomic, strong) NSString *bookingNumber;

//To get form passengerTable
@property (nonatomic, strong) NSMutableArray *passengersList;

@property (nonatomic, strong) NSString *isDispatched;

//Just get the status number: while displaying, compare and show from maint model
@property (nonatomic, strong) NSString *jobStatus;
@property (nonatomic, strong) NSString *refVehicleType;


@property (nonatomic, strong) NSDate *pickupTime;
@property (nonatomic, strong) NSString *puSuburb;
@property (nonatomic, strong) NSString *puAddress;


@property (nonatomic, strong) NSString *refChauffeur;

////Secondary Display Data////

@property (nonatomic, strong) NSString *drSuburb;
@property (nonatomic, strong) NSString *drAddress;

@property (nonatomic, strong) NSString *specialInstructions;
@property (nonatomic, strong) NSString *accountName;


@property (nonatomic, strong) NSString *allowSundries;
@property (nonatomic, strong) NSNumber *allowKm;
@property (nonatomic, strong) NSNumber *totalKM;
@property (nonatomic, strong) NSString *waitingTime;

@end
