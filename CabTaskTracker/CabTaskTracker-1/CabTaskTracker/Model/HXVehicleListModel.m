//
//  HXVehicleListModel.m
//  CabTaskTracker
//
//  Created by Mithun on 30/07/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import "HXVehicleListModel.h"

#import "HXMainModel.h"
#import "HXWebServiceManager.h"
#import "HXCoreDataManager.h"

@implementation HXVehicleListModel
{
    HXWebServiceManager *serviceManager;
    
    NSManagedObjectContext *context;
    NSEntityDescription *entityDesc;
    HXCoreDataManager* coreDataManager;
    

}

-(id)init
{
    if ((self = [super init]))
    {
        serviceManager = [[HXWebServiceManager alloc]init];
        
        //CoreData Manager
        coreDataManager = [HXCoreDataManager sharedInstance];
        context = [coreDataManager managedObjectContext];
        
    }
    return self;
}

-(void)getVehicleList
{
    [serviceManager setServiceDelegate:self];
    [serviceManager getVehicleListForUserName:[HXMainModel sharedInstance].currentUser];
}

-(void)storeVehicleListInDBWithMessage:(NSString *)message
{
    for (HXVehicleListModel *vehicleDetail in _vehicleModel.vehicleListAry)
    {
        NSError* error;
        
        //retrieval from Vehicle
        
        NSString *predicateString = [NSString stringWithFormat:@"vehicleID==\"%@\"", [NSString stringWithFormat:@"%d",[vehicleDetail.vehicleID intValue]]];
        
        
        NSArray* results = [[HXCoreDataManager sharedInstance]getMatchedRowsFromEntity:@"Vehicle" predicateString:predicateString];
        
        
        
        context = [[HXCoreDataManager sharedInstance]managedObjectContext];
        
        if (results.count>0)
        {
            for (NSManagedObject* vehicleRow in results)
            {
                if ([[NSString stringWithFormat:@"%@",vehicleDetail.vehicleID] isEqualToString:[vehicleRow valueForKey:@"vehicleID"]]) {//comparing the vehicle id ,.,if the vehicel id already exists, we need to update the same row instead of adding a new one
                    
                    [vehicleRow setValue:vehicleDetail.isVehicleActive forKey:@"isActive"];
                    [vehicleRow setValue:vehicleDetail.chauffeurID forKey:@"chauffeurID"];
                    [vehicleRow setValue:vehicleDetail.rego forKey:@"rego"];
                    [vehicleRow setValue:vehicleDetail.vehicleID forKey:@"vehicleID"];
                    [vehicleRow setValue:vehicleDetail.refVehicleType forKey:@"refVehicleType"];
                    
                    [context save:&error];
                    break;
                }
            }
        }
        else
        {
            //insertion in case vehcile id is not already there
            NSManagedObject* vehicleRow = [NSEntityDescription insertNewObjectForEntityForName:@"Vehicle" inManagedObjectContext:context];
            
            [vehicleRow setValue:vehicleDetail.isVehicleActive forKey:@"isActive"];
            [vehicleRow setValue:vehicleDetail.chauffeurID forKey:@"chauffeurID"];
            [vehicleRow setValue:vehicleDetail.rego forKey:@"rego"];
            [vehicleRow setValue:vehicleDetail.vehicleID forKey:@"vehicleID"];
            [vehicleRow setValue:vehicleDetail.refVehicleType forKey:@"refVehicleType"];
            
            [context save:&error];
            
            if (error) {
                NSLog(@"New Record not inserted in Vehicle List");
            }
            else
            {
                NSLog(@"New Record  inserted in Vehicle List");
            }
        }
    }
    [_modelDelegate storeVehicleList_Status:message];
}




//TODO: Protocal Implementation:
-(void)recievedVehicleList:(NSArray *)vehicleList
{
    //id key = [[vehicleList allKeys] objectAtIndex:0]; // Assumes 'message' is not empty
    //NSArray *recievedListAry = [vehicleList allValues];//[key objectForKey:key];
    _vehicleListAry = [[NSMutableArray alloc]init];
    
    for (NSDictionary *vehicleDetails in vehicleList)
    {
        _vehicleModel = [[HXVehicleListModel alloc]init];
        
        _vehicleModel.vehicleID = [NSString stringWithFormat:@"%@",[vehicleDetails valueForKey:@"id"]];
        _vehicleModel.chauffeurID = [[HXMainModel sharedInstance]currentUser];
        _vehicleModel.rego = [vehicleDetails valueForKey:@"rego"];
        _vehicleModel.refVehicleType = [NSString stringWithFormat:@"%@",[vehicleDetails valueForKey:@"refVehicleType"]];
        _vehicleModel.isVehicleActive = [NSString stringWithFormat:@"%@",[vehicleDetails valueForKey:@"isActive"]];
        
        [_vehicleListAry addObject:_vehicleModel];
                                         
    }
    
    
    NSDate *currDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"dd.MM.YY HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:currDate];
    
    _lastUpdateTime = dateString;
    _messageFromServer = @"OK";
    
    [self storeVehicleListInDBWithMessage:_messageFromServer];
}

-(void)recievedVehicleListFailedWithReason:(NSString *)reason
{
    _messageFromServer = reason;
    [_modelDelegate storeVehicleList_Status:reason];
    //Send the reason back and display in view:
}


@end
