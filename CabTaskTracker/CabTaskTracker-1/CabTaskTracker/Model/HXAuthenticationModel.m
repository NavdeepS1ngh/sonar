//
//  HXAuthenticationModel.m
//  CabTaskTracker
//
//  Created by Mithun on 21/07/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import "HXAuthenticationModel.h"

#import "HXMainModel.h"
#import "HXWebServiceManager.h"
#import "HXCoreDataManager.h"

@interface HXAuthenticationModel ()
{
    NSString *userName;
    NSString *password;
    
    HXWebServiceManager *serviceManager;

    NSManagedObjectContext *context;
    NSEntityDescription *entityDesc;
    HXCoreDataManager* coreDataManager;
}
@end

@implementation HXAuthenticationModel

-(void)authenticateUser
{
    if ([HXMainModel sharedInstance].isServerConnected == YES && [HXMainModel sharedInstance].isValidToken == NO) {
        
        [self getCredentialFromLocal];
        // Call WebServiceManager class- >  Login method
    }
}

-(void)getCredentialFromLocal
{
    // Get it from Local
    coreDataManager = [HXCoreDataManager sharedInstance];
    context = [coreDataManager managedObjectContext];
    entityDesc =    [NSEntityDescription entityForName:@"Chauffeur"
                                inManagedObjectContext:context];
    NSError* fetchError;
    NSFetchRequest* fetchReq = [[NSFetchRequest alloc]init];
    [fetchReq setEntity:entityDesc];
    
    NSPredicate* fetchPredicate = [NSPredicate predicateWithFormat:@"chauffeurID==%@",[[HXMainModel sharedInstance] currentUser]];
    [fetchReq setPredicate:fetchPredicate];
    
    NSArray* results = [context executeFetchRequest:fetchReq error:&fetchError];
    if (fetchError) {
        NSLog(@"Error : %@",fetchError);
    }
    else
    {// no error! SUCCESSFULLY FETCHED !
        if (results.count>0) {
            for (NSManagedObject* object in results) {
                password = [object valueForKey : @"password"];
                userName = [[HXMainModel sharedInstance]currentUser];
                break;
            }
            [context save:&fetchError];
        }
        else
        {
            //if not matching user found, control will come here
        }
    }
    
}

-(void)configureUserWithUserName:(NSString *)userID andPassword:(NSString *)psw
{
    //Recieved form configuration view
    userName = userID;
    password = psw;

    [self callAuthenticationAPI];
}

-(void)callAuthenticationAPI
{
    if([HXMainModel sharedInstance].isServerConnected)
    {
        serviceManager = [[HXWebServiceManager alloc]init];
        [serviceManager setServiceDelegate:self];
        [serviceManager sendAuthenticateUserRequestWithUserName:userName password:password];
    }
    else
    {
        //Call some Protocol
    }
}

//ServiceManagerProtocol Implementation
-(void)authenticationResult:(BOOL)isSucess
{
    if (isSucess) {
        [_modelDelegate saveCredentialInDB];
    }
    else
    {
        [HXMainModel sharedInstance].isValidToken = NO;
        [_modelDelegate authenticationErrorDispaly];
    }
}

@end
