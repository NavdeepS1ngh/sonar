//
//  HXMainModel.m
//  CabTaskTracker
//
//  Created by Mithun on 21/07/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import "HXMainModel.h"

#import <CommonCrypto/CommonHMAC.h>
#import "NSData+Base64.h"
#define salt @"TransElite"

#import "HXCoreDataManager.h"

@implementation HXMainModel

static  HXMainModel* sharedInstance=nil;

+(HXMainModel*)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[HXMainModel alloc] init];
    });
    
    return sharedInstance;
    
}

//Default Init
- (id)init {
    if (self = [super init]) {
        // Init for any default value:
        
        //Assumed that: first time, the server is connected:
        //If app unable to fetch response for any request, thereafter it is changed into NO
        [self setIsServerConnected:YES];
        
    }
    return self;
}

//Custome Init -> For configuration setup

-(id)initForConfig {
    if (self = [super init]) {
        
        //Assumed that: first time, the server is connected:
        //If app unable to fetch response for any request, thereafter it is changed into NO
        [self setIsServerConnected:YES];
        
        _authenticateModel = [[HXAuthenticationModel alloc]init];
    }
    return self;
}

#pragma mark - Utility Methods

+(void)saveSyncMaintenanceKey:(NSString*)syncKey
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    if (standardUserDefaults) {
        [standardUserDefaults setObject:syncKey forKey:@"synMaintKey"];
        [standardUserDefaults synchronize];
    }
}
+(NSString *)syncMaintenanceKey
{
    return [[NSUserDefaults standardUserDefaults]
            stringForKey:@"synMaintKey"];
}

+(void)saveSyncOpsDateKey:(NSString*)syncKey
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    if (standardUserDefaults) {
        [standardUserDefaults setObject:syncKey forKey:@"synOpsDateKey"];
        [standardUserDefaults synchronize];
    }
}
+(NSString *)syncOpsDateKey
{
    return [[NSUserDefaults standardUserDefaults]
            stringForKey:@"synOpsDateKey"];
}

+(void)saveNewToken:(NSString*)recievedToken
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    if (standardUserDefaults) {
        [standardUserDefaults setObject:recievedToken forKey:@"token"];
        [standardUserDefaults synchronize];
    }
}

+(NSString *)currentToken
{
    return [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"token"];
}

+(NSString*)getEncryptedPasswordFor:(NSString*)pswString
{
    NSString *pswNsalt = [NSString stringWithFormat:@"%@%@",pswString,salt];
    const char *pswCstr = [pswNsalt cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *pswData = [NSData dataWithBytes:pswCstr length:pswNsalt.length];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    CC_SHA1(pswData.bytes, pswData.length, digest);
    
    NSData *HMAC = [[NSData alloc] initWithBytes:digest length:sizeof(digest)];
    NSString *hashPsw = [HMAC base64EncodedString];
    
    return hashPsw;
    
}

#pragma mark - Methods Implementation
-(void)getVehicleList
{
    _vehicleModel = [[HXVehicleListModel alloc]init];
    [_vehicleModel setModelDelegate:self];
    [_vehicleModel getVehicleList];
    
}
-(NSMutableArray*) getVehicleIDFromDB
{
    NSMutableArray *recievedVehicleList = [[NSMutableArray alloc]init];
    
    NSArray* results = [[HXCoreDataManager sharedInstance]getMatchedRowsFromEntity:@"Vehicle" predicateString:@""];
    
    if (results.count>0)
    {
        for (NSManagedObject* vehicleRow in results)
        {
            NSString* vehicleId = [vehicleRow valueForKey:@"vehicleID"];
            
            [recievedVehicleList addObject:vehicleId];
            
        }
    }
    else
    {
        //NO data present in Vehicle table
    }
    
    return recievedVehicleList;
}

-(void)getMaintenanceData
{
    _maintenanceModel = [[HXMaintenanceModel alloc]init];
    [_maintenanceModel setModelDelegate:self];
    [_maintenanceModel getMaintenanceData];
}

-(void)getOperationalData
{
    _operationalModel = [[HXOperationalModel alloc]init];
    [_operationalModel setModelDelegate:self];
    [_operationalModel getOpsData];
}

//From Local DB
-(void)fetchDataForJobDetails
{
    _jobDetailsModel = [[HXJobDetailsModel alloc]init];
    //[_jobDetailsModel setModelDelegate:self];
    [_jobDetailsModel loadJobDetailsFromDB];
    
    //Protocol Implementation temp solution
    [_controllerDelegate requiredDataFetch_Result:@"OK"];
}

-(void)fetchLegAndAddtionalRequestForBooking:(NSString *)refBooking
{
    _additionalJobDetailsModel = [[HXAdditionalJobDetailsModel alloc]init];
    [_additionalJobDetailsModel setRefBookingID:refBooking];
    [_additionalJobDetailsModel loadAdditionalData];
    
}

-(void)fetchPredefinedListsFromMaintenanceDB
{
    //Load allJobStatusList;
    //Load allSundriesList;
    //Load allAdditionalRequestList;
    
    HXMaintenanceModel *maintObjForLocalFetch;
    maintObjForLocalFetch = [[HXMaintenanceModel alloc]init];
    
    _allJobStatusList = [maintObjForLocalFetch fetchAllJobStatusListFromDB];
    _allSundriesList = [maintObjForLocalFetch fetchAllSundriesListFromDB];
    _allAdditionalRequestList = [maintObjForLocalFetch fetchAllRequestListFromDB];
    _allVehicleList = [maintObjForLocalFetch fetchAllVehicleListFromDB];
    
}

#pragma mark - Protocol Delegate Implementation
-(void)storeVehicleList_Status:(NSString *)message
{
    if ([message isEqualToString: @"OK"])
    {
        
    }
    [_controllerDelegate vehicleListDisplayWithStatus:message];
}

-(void)storeMaintenanceList_Status:(NSString *)message
{
    if ([message isEqualToString:@"OK"]) {
        
        //TODO: Replace the code later; call Ops:
        [self getOperationalData];
    }
    else
    {
        [_controllerDelegate requiredDataFetch_Result:message];
    }
    
}

-(void)storeOpsList_Status:(NSString *)message
{
    if ([message isEqualToString:@"OK"]) {
        
        //Refresh JobDetailsModel with updated data in DB:
        [self fetchDataForJobDetails];
    }
    else
    {
        [_controllerDelegate requiredDataFetch_Result:message];
    }

}

#pragma mark - Rechability Methods
-(void)initiateNetworkTracker
{
    /*
     Observe the kNetworkReachabilityChangedNotification. When that notification is posted, the method reachabilityChanged will be called.
     */
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    
    
    self.internetReachability = [Reachability reachabilityForInternetConnection];
	[self.internetReachability startNotifier];
	[self updateParametersWithRechability:self.internetReachability];
    
}


/*!
 * Called by Reachability whenever status changes.
 */

- (void) reachabilityChanged:(NSNotification *)note
{
	Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
	[self updateParametersWithRechability:curReach];
}

- (void)updateParametersWithRechability:(Reachability *)reachability
{

    NetworkStatus netStatus = [reachability currentReachabilityStatus];
    
	if (reachability == self.internetReachability)
	{
        switch (netStatus) {
            case NotReachable:
                _isNetworkConnected = NO;
                break;
            case ReachableViaWiFi:
                _isNetworkConnected = YES;
                break;
            case ReachableViaWWAN:
                _isNetworkConnected = YES;
                break;
        }
	}
}

-(void)stopNetworkTracker
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}




@end
