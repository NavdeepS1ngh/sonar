//
//  HXAuthenticationModel.h
//  CabTaskTracker
//
//  Created by Mithun on 21/07/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "HXModelMgrProtocol.h"
#import "HXServiceMgrProtocol.h"

@interface HXAuthenticationModel : NSObject <HXServiceMgrProtocol>

@property BOOL isFromConfigurationMode;

@property(readwrite,assign)id<HXModelMgrProtocol>modelDelegate;

//authenticationResult


-(void)configureUserWithUserName:(NSString *)userID andPassword:(NSString *)psw;

@end

