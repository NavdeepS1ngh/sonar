//
//  HXAppDelegate.h
//  CabTaskTracker
//
//  Created by Mithun R on 26/06/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HXAppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;

@end




