//
//  TableMenuHelpers.m
//  CabTaskTracker
//
//  Created by Navdeep  Singh on 8/12/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import "TableMenuHelpers.h"

@implementation TableMenuItem
@synthesize imagePath;
@synthesize text;
@synthesize selector;
-(id) init
{
    self = [super init];
    if (self)
    {
        self.imagePath = nil;
        self.text = @"";
    }
    return self;
}
@end

@implementation TableMenuSection
@synthesize items;
@synthesize name;
-(id) init
{
    self = [super init];
    if (self)
    {
        self.items = [[NSMutableArray alloc] init];
        self.name = @"";
    }
    return self;
}

@end
