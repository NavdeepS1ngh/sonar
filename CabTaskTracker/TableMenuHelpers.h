//
//  TableMenuHelpers.h
//  CabTaskTracker
//
//  Created by Navdeep  Singh on 8/12/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TableMenuHelpers : NSObject


@end
@interface TableMenuItem : NSObject
@property (nonatomic, strong) NSString *imagePath;
@property (nonatomic, strong) NSString *text;
@property (nonatomic) SEL selector;
@end

@interface TableMenuSection : NSObject
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) NSString *name;


@end
