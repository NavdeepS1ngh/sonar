//
//  HXCoreDataManager.h
//  CabTaskTracker
//
//  Created by aditi on 21/07/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HXCoreDataManager : NSObject
{
@private
    NSManagedObjectContext *managedObjectContext;
    NSManagedObjectModel *managedObjectModel;
    NSPersistentStoreCoordinator *persistentStoreCoordinator;
}
@property (strong, nonatomic) UIWindow *window;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
-(NSArray*) getMatchedRowsFromEntity : (NSString*)entityName predicateString:(NSString*)predicateString
;

+(HXCoreDataManager*)sharedInstance;
@end
