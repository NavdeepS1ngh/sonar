//
//  HXWebServiceManager.m
//  CustomerOnAR
//
//  Created by aditi on 21/07/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import "HXWebServiceManager.h" 

#import "HXMainModel.h"

#define AUTHKEY @"T8rqHUNFWuncssSIGtDFthVjPRHRU7dADd1n"
#define URL @"https://203.222.65.186/TransElite/"
#define hostname @"https://203.222.65.186/"


@implementation HXWebServiceManager
{
    SecCertificateRef certificate;
    
    BOOL stopAuthenticationServiceFlow;
    BOOL stopGetVhListServiceFlow;
    BOOL stopGetMaintenanceServiceFlow;
    BOOL stopGetOpsServiceFlow;
}

-(void) sendAuthenticateUserRequestWithUserName:(NSString*)userName password:(NSString*)password
{
    
    connectionType = EAUTHENTICATEUSER ;
    
    NSLog(@"<<<< %s Entering",__PRETTY_FUNCTION__);
    
    NSString* requestBody =[NSString stringWithFormat:@"grant_type=password&username=%@&password=%@&key=%@",userName,password,AUTHKEY];
    
    NSString *trim = [[NSString stringWithFormat:@"%@Authenticate",URL] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL* url = [NSURL URLWithString:trim];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
    
    [request addValue: @"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody: [requestBody dataUsingEncoding:NSUTF8StringEncoding]];
    
    //Starts the http connection
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [theConnection start];
    
//    NSURLResponse *response;
//    NSData *GETReply = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
//    NSString *theReply = [[NSString alloc] initWithBytes:[GETReply bytes] length:[GETReply length] encoding: NSASCIIStringEncoding];
//    NSLog(@"Reply: %@", theReply);

}

-(void) getVehicleListForUserName:(NSString*)userName
{
    connectionType = EGETVEHICLELIST ;
    
    NSLog(@"<<<< %s Entering",__PRETTY_FUNCTION__);
    
    NSString *trim = [[NSString stringWithFormat:@"%@api/Vehicles/",URL] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL* url = [NSURL URLWithString:trim];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *tokenValue = [NSString stringWithFormat:@"Bearer %@", [HXMainModel currentToken]];
    
    [request addValue: @"application/json" forHTTPHeaderField:@"Accept"];
    [request addValue: tokenValue forHTTPHeaderField:@"Authorization"];
    [request setHTTPMethod:@"GET"];
    
    //Starts the http connection
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [theConnection start];
}

-(void) sendGetMessageRequest
{
    NSLog(@"<<<< %s Entering",__PRETTY_FUNCTION__);
    
    connectionType = EGETMESSAGE;
    
    NSString *trim = [[NSString stringWithFormat:@"%@api/Messages",URL] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL* url = [NSURL URLWithString:trim];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
    [request addValue: @"application/json" forHTTPHeaderField:@"Accept"];
    [request addValue: @"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"GET"];
    
    NSString *tokenValue = [NSString stringWithFormat:@"Bearer %@", [HXMainModel currentToken]];
    
    [request setValue:tokenValue forHTTPHeaderField:@"Authorization"];
    //    [request setHTTPBody: [requestBody dataUsingEncoding:NSUTF8StringEncoding]];
    
    //Starts the http connection
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [theConnection start];
    
}
-(void) sendPostMessageRequest
{
    NSLog(@"<<<< %s Entering",__PRETTY_FUNCTION__);
    
    connectionType = EPOSTMESSAGE;
    
    NSString *trim = [[NSString stringWithFormat:@"%@api/Messages",URL] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL* url = [NSURL URLWithString:trim];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
    [request addValue: @"application/json" forHTTPHeaderField:@"Accept"];
    [request addValue: @"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"POST"];
    
    NSString *tokenValue = [NSString stringWithFormat:@"Bearer %@", [HXMainModel currentToken]];
    
    [request setValue:tokenValue forHTTPHeaderField:@"Authorization"];
    //request body
    NSArray *keys = [NSArray arrayWithObjects: @"refChauffeur",@"senderName",@"messageText",@"createdOn", nil];
    NSArray *objects = [NSArray arrayWithObjects:[NSNumber numberWithInt:7],@"Sender",@"Messgae Text",@"2014-07-21T14:49:24.983", nil];
    NSDictionary *jsonDictionary = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary
                                                       options:0
                                                         error:nil];
    [request setHTTPBody:jsonData];
    ///
    //Starts the http connection
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [theConnection start];
    
}

-(void)getMaintenanceRequest
{
    NSLog(@"<<<< %s Entering",__PRETTY_FUNCTION__);
    
    connectionType = EMAINTENANCE;
    
    NSString *trim = [[NSString stringWithFormat:@"%@api/Maintenance",URL] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL* url = [NSURL URLWithString:trim];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
    [request addValue: @"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:@"GET"];
    
    //NSString *tokenValue = [NSString stringWithFormat:@"Bearer %@", [HXMainModel currentToken]];
    NSString *tokenValue = [NSString stringWithFormat:@"Bearer %@", [HXMainModel currentToken]];
    
    [request setValue:tokenValue forHTTPHeaderField:@"Authorization"];
    
    
    ///
    //Starts the http connection
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [theConnection start];

}

-(void)getOpsDataRequest
{
    NSLog(@"<<<< %s Entering",__PRETTY_FUNCTION__);
    
    connectionType = EGETOPSDATA;
    
    NSString *trim = [[NSString stringWithFormat:@"%@api/Ops/read",URL] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL* url = [NSURL URLWithString:trim];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
    [request addValue: @"application/json" forHTTPHeaderField:@"Accept"];
    [request addValue: @"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"POST"];
    
    NSString *tokenValue = [NSString stringWithFormat:@"Bearer %@", [HXMainModel currentToken]];
    
    [request setValue:tokenValue forHTTPHeaderField:@"Authorization"];
    //request body
    NSArray *keys = [NSArray arrayWithObjects: @"syncKey",@"syncDate",@"bookingIdList", nil];
    
    NSString *opsSyncKey;
    if ([HXMainModel syncOpsDateKey] == (id)[NSNull null] || [HXMainModel syncOpsDateKey] == nil)
        opsSyncKey = @"";
    else
        opsSyncKey = [HXMainModel syncOpsDateKey];
    
    //NSMutableArray *availableBookingList;
    
    NSArray *objects = [NSArray arrayWithObjects:opsSyncKey,[HXMainModel syncMaintenanceKey],[NSArray arrayWithObjects:[NSNumber numberWithInt:1001],[NSNumber numberWithInt:1002], nil], nil];
    NSDictionary *jsonDictionary = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary
                                                       options:0
                                                         error:nil];
    [request setHTTPBody:jsonData];
    ///
    //Starts the http connection
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [theConnection start];
    
}

-(void)postOpsDataRequest
{
    NSLog(@"<<<< %s Entering",__PRETTY_FUNCTION__);
    
    connectionType = EPOSTOPSDATA;
    
    NSString *trim = [[NSString stringWithFormat:@"%@api/ops/save",URL] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL* url = [NSURL URLWithString:trim];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
    [request addValue: @"application/json" forHTTPHeaderField:@"Accept"];
    [request addValue: @"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"POST"];
    
    NSString *tokenValue = [NSString stringWithFormat:@"Bearer %@", [HXMainModel currentToken]];
    
    [request setValue:tokenValue forHTTPHeaderField:@"Authorization"];
    //request body
    NSArray *keys = [NSArray arrayWithObjects: @"chauffeurStatusLogs",@"bookingStatusLogs",@"bookingResults",@"bookingSundryResults", nil];
    NSArray *objects = [NSArray arrayWithObjects:[NSNull null],[NSNull null],[NSNull null],[NSNull null], nil];
    NSDictionary *jsonDictionary = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary
                                                       options:0
                                                         error:nil];
    [request setHTTPBody:jsonData];
    ///
    //Starts the http connection
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [theConnection start];

}

#pragma mark - NSURLConnectionDelegate

- (BOOL)shouldTrustProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
    // Load up the bundled certificate.
    NSBundle* bundle = [NSBundle bundleForClass:[self class]];
    // NSString *certPath = [[NSBundle mainBundle] pathForResource:@"TransEliteCA" ofType:@"der"];
    NSData *certData = [NSData dataWithContentsOfFile:[bundle pathForResource:@"TransEliteCA" ofType:@"der"]];
    // CFDataRef certDataRef = (__bridge_retained CFDataRef)certData;
    SecCertificateRef cert = SecCertificateCreateWithData(NULL, ( CFDataRef)CFBridgingRetain(certData));
    certificate = cert;

    
    // Establish a chain of trust anchored on our bundled certificate.
    CFArrayRef certArrayRef = CFArrayCreate(NULL, (void *)&cert, 1, NULL);
    SecTrustRef serverTrust = protectionSpace.serverTrust;
    SecTrustSetAnchorCertificates(serverTrust, certArrayRef);
    
    // Verify that trust.
    SecTrustResultType trustResult;
    SecTrustEvaluate(serverTrust, &trustResult);
    
    // Clean up.
    CFRelease(certArrayRef);
    CFRelease(cert);
    //    CFRelease(certDataRef);
    
	// Did our custom trust chain evaluate successfully?
    return trustResult  ;
}


-(BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    if ([self shouldTrustProtectionSpace:challenge.protectionSpace]) {
        [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
    } else {
        [challenge.sender performDefaultHandlingForAuthenticationChallenge:challenge];
    }
}
//
//- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
//    [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
//}


- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError :error : %@",error.description);
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    int code = [httpResponse statusCode]; // To track response Code:
    
    switch (connectionType) {
        case EAUTHENTICATEUSER:
        {
            NSString *errorDsc = [NSHTTPURLResponse localizedStringForStatusCode:code];
            
            if (code == EOK) {
                stopAuthenticationServiceFlow = NO;
                // Retry authentication
            }
            else if (code == EBADREQUEST)
            {
                //TODO: Pending-> To figure out error dsc for error code
                if ([errorDsc isEqualToString:@"API Key is missing"]) {
                    //
                }
                else if ([errorDsc isEqualToString:@"Invalid API Key"]) {
                    //
                }
                else if ([errorDsc hasPrefix:@"Database Exception:"]) {
                    //
                }
                else if ([errorDsc isEqualToString:@"The user name or password is incorrect"]) {
                    //
                }
                stopAuthenticationServiceFlow = YES;
                [_serviceDelegate authenticationResult:NO];
            }
        }
            break;
        case EGETVEHICLELIST:
        {
            if (code == EOK) {
                stopGetVhListServiceFlow = NO;
                [[HXMainModel sharedInstance]setIsValidToken : YES];
            }
            else if (code == EUNAUTHORIZED) {
                [[HXMainModel sharedInstance]setIsValidToken : NO];
                stopGetVhListServiceFlow = YES;
                [_serviceDelegate recievedVehicleListFailedWithReason:@"UNAUTHORIZED"];
                // Retry authentication
                //Change Token Status
                //call Authentication
            }
            else if (code == EFOUND) {
                stopGetVhListServiceFlow = YES;
                [_serviceDelegate recievedVehicleListFailedWithReason:@"FOUND"];
                //Https is required to do the request
            }
            else if (code == EINTERNALSERVERERROR) {
                stopGetVhListServiceFlow = YES;
                [_serviceDelegate recievedVehicleListFailedWithReason:@"INTERNALSERVERERROR"];
                //
            }
            else if (code == ENOTFOUNT) {
                stopGetVhListServiceFlow = YES;
                [_serviceDelegate recievedVehicleListFailedWithReason:@"ENOTFOUNT"];
                //
            }
            else if (code == EBADREQUEST) {
                stopGetVhListServiceFlow = YES;
                [_serviceDelegate recievedVehicleListFailedWithReason:@"BADREQUEST"];
                //
            }
        }
            break;
        case EMAINTENANCE:
        {
            if (code == EOK) {
                stopGetMaintenanceServiceFlow = NO;
                [[HXMainModel sharedInstance]setIsValidToken : YES];
            }
            else if (code == EUNAUTHORIZED) {
                [[HXMainModel sharedInstance]setIsValidToken : NO];
                stopGetMaintenanceServiceFlow = YES;
                [_serviceDelegate recievedMaintenanceFailedWithReason:@"UNAUTHORIZED"];
                
                // Retry authentication
                //Change Token Status
                //call Authentication
            }
            else if (code == EFOUND) {
                stopGetMaintenanceServiceFlow = YES;
                [_serviceDelegate recievedMaintenanceFailedWithReason:@"FOUND"];
                //Https is required to do the request
            }
            else if (code == EINTERNALSERVERERROR) {
                stopGetMaintenanceServiceFlow = YES;
                [_serviceDelegate recievedMaintenanceFailedWithReason:@"INTERNALSERVERERROR"];
                //
            }
            else if (code == ENOTFOUNT) {
                stopGetMaintenanceServiceFlow = YES;
                [_serviceDelegate recievedMaintenanceFailedWithReason:@"ENOTFOUNT"];
                //
            }
            else if (code == EBADREQUEST) {
                stopGetMaintenanceServiceFlow = YES;
                [_serviceDelegate recievedMaintenanceFailedWithReason:@"BADREQUEST"];
                //
            }
        }
            break;
        case EGETOPSDATA:
        {
            if (code == EOK) {
                stopGetOpsServiceFlow = NO;
                [[HXMainModel sharedInstance]setIsValidToken : YES];
            }
            else if (code == EUNAUTHORIZED) {
                [[HXMainModel sharedInstance]setIsValidToken : NO];
                stopGetOpsServiceFlow = YES;
                [_serviceDelegate recievedOpsFailedWithReason:@"UNAUTHORIZED"];
                
                // Retry authentication
                //Change Token Status
                //call Authentication
            }
            else if (code == EFOUND) {
                stopGetOpsServiceFlow = YES;
                [_serviceDelegate recievedOpsFailedWithReason:@"FOUND"];
                //Https is required to do the request
            }
            else if (code == EINTERNALSERVERERROR) {
                stopGetOpsServiceFlow = YES;
                [_serviceDelegate recievedOpsFailedWithReason:@"INTERNALSERVERERROR"];
                //
            }
            else if (code == ENOTFOUNT) {
                stopGetOpsServiceFlow = YES;
                [_serviceDelegate recievedOpsFailedWithReason:@"ENOTFOUNT"];
                //
            }
            else if (code == EBADREQUEST) {
                stopGetOpsServiceFlow = YES;
                [_serviceDelegate recievedOpsFailedWithReason:@"BADREQUEST"];
                //
            }
        }
            break;
        case EPOSTOPSDATA:
        {
        }
            break;
        default:
            break;
    }
    
    webData=[NSMutableData data];
    [webData setLength:0];
}
    
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [webData appendData:data];
    
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    switch (connectionType) {
        case EAUTHENTICATEUSER:
        {
            if (stopAuthenticationServiceFlow == NO) {
                NSString *result = [[NSString alloc] initWithData:webData encoding:NSUTF8StringEncoding];
                NSLog(@"connectionDidFinishLoading : EAUTHENTICATEUSER : %@",result);
                
                //retrieving the accesss token
                NSDictionary *recievedDictionary=[NSJSONSerialization
                                             JSONObjectWithData:webData
                                             options:NSJSONReadingMutableLeaves
                                             error:nil];

                
                [HXMainModel saveNewToken:[recievedDictionary valueForKey:@"access_token"]];
                
                //Call Delegate:
                [_serviceDelegate authenticationResult:YES];
                
             
            }
            // Do Nothing for else -> Handel it inside didReceiveResponse method
        }
        break;
        case EGETVEHICLELIST:
        {
            if (stopGetVhListServiceFlow == NO) {
                NSString *result = [[NSString alloc] initWithData:webData encoding:NSUTF8StringEncoding];
                NSLog(@"connectionDidFinishLoading : EGETMESSAGE : %@",result);
                
                //NSDictionary *recievedDictionary= [NSJSONSerialization JSONObjectWithData:webData options:NSJSONReadingMutableContainers error:nil];
                NSArray *recievedArray= [NSJSONSerialization JSONObjectWithData:webData options:NSJSONReadingMutableContainers error:nil];
                
                [_serviceDelegate recievedVehicleList:recievedArray];
            }
            // Do Nothing for else -> Handel it inside didReceiveResponse method
        }
        break;
        case EGETMESSAGE:
        {
            NSString *result = [[NSString alloc] initWithData:webData encoding:NSUTF8StringEncoding];
            NSLog(@"connectionDidFinishLoading : EGETMESSAGE : %@",result);
        }
        break;
        case EPOSTMESSAGE:
        {
            NSString *result = [[NSString alloc] initWithData:webData encoding:NSUTF8StringEncoding];
            NSLog(@"connectionDidFinishLoading : EPOSTMESSAGE : %@",result);
        }
        break;
        case EMAINTENANCE:
        {
            if (stopGetMaintenanceServiceFlow == NO) {
                NSString *result = [[NSString alloc] initWithData:webData encoding:NSUTF8StringEncoding];
                NSLog(@"connectionDidFinishLoading : EGETMESSAGE : %@",result);
            
                NSDictionary *recievedDictionary= [NSJSONSerialization JSONObjectWithData:webData options:NSJSONReadingMutableContainers error:nil];
                [_serviceDelegate recievedMaintenanceData:recievedDictionary];

            }
            // Do Nothing for else -> Handel it inside didReceiveResponse method
        }
        break;
        case EGETOPSDATA:
        {           
            if (stopGetOpsServiceFlow == NO) {
                NSString *result = [[NSString alloc] initWithData:webData encoding:NSUTF8StringEncoding];
                NSLog(@"connectionDidFinishLoading : EGETMESSAGE : %@",result);
                
                NSDictionary *recievedDictionary= [NSJSONSerialization JSONObjectWithData:webData options:NSJSONReadingMutableContainers error:nil];
                [_serviceDelegate recievedOpsData:recievedDictionary];
                
            }
            // Do Nothing for else -> Handel it inside didReceiveResponse method
            
        }
            break;
        case EPOSTOPSDATA:
        {
            NSString *result = [[NSString alloc] initWithData:webData encoding:NSUTF8StringEncoding];
            NSLog(@"connectionDidFinishLoading : EPOSTOPSDATA : %@",result);
        }
        break;

    }
}
@end
