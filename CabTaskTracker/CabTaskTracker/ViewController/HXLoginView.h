//
//  HXLoginView.h
//  WireFrames
//
//  Created by Mithun R on 26/06/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NIDropDown.h"

#import "HXControllerMgrProtocol.h"
#import "HXModelMgrProtocol.h"

@interface HXLoginView : UIViewController<UITextFieldDelegate,NIDropDownDelegate, HXControllerMgrProtocol, HXModelMgrProtocol>{
    
    NSString *m_username;
    NSString *m_password;
    NSString *m_vehicleid;
    
    NSString *rememberName;
    NSString *rememberPwd;
    NSString *rememberVehicleid;
    
    NIDropDown *dropdown;
    
}

@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;


@property(strong,nonatomic)IBOutlet UIView *subView;
@property(strong,nonatomic)IBOutlet UIImageView *imgLogo;
@property(strong,nonatomic)IBOutlet UIButton *btnLogin;

@property (strong, nonatomic) IBOutlet UITextField *tbChauffurID;
@property (strong, nonatomic) IBOutlet UIButton *btnVehichleId;
@property (weak, nonatomic) IBOutlet UIButton *btnVhRefresh;


@property(strong,nonatomic)IBOutlet UITextField *txtPassword;

-(void)loginValidation;

- (IBAction)btnRefreshAction:(id)sender;
- (IBAction)btnLoginAction:(id)sender;
- (IBAction)btnOpenDropdownAction:(id)sender;

- (void)textFieldDidEndEditing:(UITextField *)textField ;


@end
