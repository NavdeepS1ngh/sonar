//
//  HXJobExtrasVC.m
//  CabTaskTracker
//
//  Created by Mithun R on 04/07/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import "HXJobExtrasVC.h"

#import "HXExtrasCell.h"
#import "HXTabViewController.h"
#import "CloudEftposSDK/CloudEftposSDK.h"

@interface HXJobExtrasVC ()
{
    //UITextField *activeField;
    
}
@end

@implementation HXJobExtrasVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self loadTableData];
    
    
    //For KeyBoard Notification.
    [self registerForKeyboardNotifications];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Method Implementation
-(void)loadTableData
{
        _titleList = [[NSArray alloc]initWithObjects:@"Toll",@"Parking",@"Baby Seat",@"Cleaning",@"Water / News Paper",@"Phone",@"Gratuities",@"Amount Collected",@"Others",@"Total KMs",@"Waiting Time (Min)", nil];
}
//Notification for keyboard
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

#pragma mark - Notification Implementation

- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets;
    if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])) {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.height), 0.0);
    } else {
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.width), 0.0);
    }
    
    _tblExtras.contentInset = contentInsets;
    _tblExtras.scrollIndicatorInsets = contentInsets;
    //[_tblDisplayList scrollToRowAtIndexPath:self.editingIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    _tblExtras.contentInset = UIEdgeInsetsZero;
    _tblExtras.scrollIndicatorInsets = UIEdgeInsetsZero;
}


#pragma mark - Text View delegate

/*
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}
 */

// Touch detect

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

#pragma mark - Table View delegate


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    static NSString *CellIdentifier = @"ExtrasCell";
    
    HXExtrasCell *cell = (HXExtrasCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell.lblTitle.text = _titleList[indexPath.row];
    cell.tag = [indexPath row];
    
    return cell;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return _titleList.count;
    
}

#pragma mark - Action Implementation

- (IBAction)btnBackAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

//- (IBAction)btnPaymentAction:(id)sender {
//    NSUserDefaults *currentJob = [NSUserDefaults standardUserDefaults];
//    [currentJob setObject:@"Completed" forKey:@"jobStatus"];
//    //[self.navigationController popToViewController: animated:];
//    
//    NSArray *viewControllers = [[self navigationController] viewControllers];
//    for( int i=0;i<[viewControllers count];i++){
//        id vc=[viewControllers objectAtIndex:i];
//        if([vc isKindOfClass:[HXTabViewController class]]){
//            [[self navigationController] popToViewController:vc animated:YES];
//            return;
//        }
//   }

//}
@end
