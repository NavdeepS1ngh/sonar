//
//  HXJobLegsVC.m
//
//  Created by Mithun R on 03/07/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import "HXJobLegsVC.h"
#import "HXCustomJobsLeg.h"

@interface HXJobLegsVC ()

@end

@implementation HXJobLegsVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadTblData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Method Implementation

-(void)loadTblData
{
    _legsArray = [[NSMutableArray alloc]initWithObjects:@"1",@"2", nil];
    _txtArray = [[NSMutableArray alloc]initWithObjects:@"RB Road",@"TST Road", nil];
    _suburbArray = [[NSMutableArray alloc]initWithObjects:@"Lurnea",@"Berowra", nil];
    _mobileArray = [[NSMutableArray alloc]initWithObjects:@"+91 9824868256",@"+91 9365824458", nil];
}

- (void) detailInfo:(UIButton *)sender
{
    NSLog(@"Call Button clicked for");
    
    //CustomJobsLeg *ownerCell = (CustomJobsLeg *)[[sender superview] superview];
   // NSIndexPath *indexPath = [self.tblJobsDetails indexPathForCell:ownerCell];
    
    // Country *country = [[Country alloc] init];
    // country = [self.countryList objectAtIndex:indexPath.row];
    // NSLog(@"Country is %i %@", indexPath.row, country.name);
}

#pragma mark - Method TableView - Delegate and Data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [self.legsArray count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    static NSString *CellIdentifier = @"CustomJobsLeg";
    HXCustomJobsLeg *legsCell = (HXCustomJobsLeg *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    //populate data from your country object to table view cell
    legsCell.lblLegNo.text = [self.legsArray objectAtIndex:indexPath.row];
    legsCell.lblInfoStreet.text =[self.txtArray objectAtIndex:indexPath.row];
    legsCell.lblSubuab.text = [self.suburbArray objectAtIndex:indexPath.row];
    legsCell.lblMobileNumber.text =  [self.mobileArray objectAtIndex:indexPath.row];;
    [legsCell.btnCallbutton addTarget:self action:@selector(detailInfo:)forControlEvents:UIControlEventTouchUpInside];
    
    return legsCell;
}

#pragma mark - Action TableView

- (IBAction)btnBackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
