//
//  HXViewMgrProtocol.h
//  CabTaskTracker
//
//  Created by Mithun on 04/08/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HXControllerMgrProtocol <NSObject>

-(void)vehicleListDisplayWithStatus:(NSString *)message;

//temp COde
-(void)requiredDataFetch_Result:(NSString *)message;

@end
