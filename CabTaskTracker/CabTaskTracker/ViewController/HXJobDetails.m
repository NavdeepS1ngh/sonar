//
//  HXJobDetails.m
//  ETG_App
//
//  Created by aditi on 02/07/14.
//  Copyright (c) 2014 hexaware. All rights reserved.
//

#import "HXJobDetails.h"
#import "HXAdditionalReqCell.h"

#import "HXTableCellType1.h"
#import "HXJobLegsVC.h"

#import "HXMainModel.h"
#import "HXJobDetailsModel.h"

@interface HXJobDetails ()
{
    BOOL additionalReqViewVisible;
    BOOL allTicked;
    
    NSString *currentStatus;
    
    HXJobDetailsModel *jobdetail;
    NSString *pasengerNameDisplay;
    
    NSMutableArray* respectiveAdditionalReqStringList;
}
@end

@implementation HXJobDetails
@synthesize additionalRequests;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    additionalReqViewVisible = false;
    allTicked = false;
    [self createTblData];
    
    //Temp code for flow
    [self retriveCurrentStatus];
    [self loadUI];
    
    jobdetail = [HXMainModel sharedInstance].jobDetailsList[[HXMainModel sharedInstance].jobDetailArrayIndex];
    respectiveAdditionalReqStringList = [[NSMutableArray alloc]init];
    //Load all the additional job Details of selected Job:
    [[HXMainModel sharedInstance]fetchLegAndAddtionalRequestForBooking:[HXMainModel sharedInstance].selectedRefBookingID];
}
-(void)viewWillAppear:(BOOL)animated
{
    //loading additional requests
    static int count = 0;
    
    NSMutableArray* respectiveAdditionalReqObjList = [[[HXMainModel sharedInstance] additionalJobDetailsModel] additionalRequestList];
    if (respectiveAdditionalReqObjList.count>0) {
        for (HXMaintenanceUtilModel* additionalRequest in [[HXMainModel sharedInstance] allAdditionalRequestList]) {
            
            if ([additionalRequest.utilID isEqualToString:respectiveAdditionalReqObjList[count]]) {
                [respectiveAdditionalReqStringList addObject:additionalRequest.utilDescription];
                count++;
                break;
            }
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Method Implementation

-(void)createTblData
{
    _titleList = [[NSArray alloc]initWithObjects:@"Job ID",@"PU Time",@"Account",@"Passenger(s)",@"PU Address",@"DR Address",@"Spl Instrns",@"Vehicle Type",@"Legs", nil];
    //_contentList = [[NSArray alloc]initWithObjects:@"10192",@"10:50 AM, 8th July",@"Hughes Corporate Text",@"John Ric + 3 more",@"North Mead",@"Sydney",@"1 Instructions available",@"C Class",@"2 Legs",nil];
    _navigationStatus = [[NSArray alloc]initWithObjects:@"N",@"N",@"N",@"P",@"P",@"P",@"P",@"N",@"Y", nil];
    additionalRequests = [[NSArray alloc]initWithObjects:@"Arrive at the gate 10 min prior to pick up",@"", nil];
    
    
    if (jobdetail.passengersList.count > 1)
    {
        pasengerNameDisplay = [NSString stringWithFormat:@"%@ & %i More", jobdetail.passengersList[0], jobdetail.passengersList.count];
    }
    else if (jobdetail.passengersList.count == 0)
    {
        pasengerNameDisplay = @"";
    }
    else
    {
        pasengerNameDisplay = jobdetail.passengersList[0];
    }

    
}

-(void)createAlertWithTitle:(NSString *)alertTitle andMessage:(NSString *)alertMessage
{
    UIAlertView *popAlert = [[UIAlertView alloc]initWithTitle:alertTitle message:alertMessage delegate:self cancelButtonTitle:@"Close" otherButtonTitles: nil];
    [popAlert show];
}

-(void) checkboxTicked : (id)sender
{
    //changing the state of checkbox for slected row
    HXAdditionalReqCell *additinalReq =(HXAdditionalReqCell *)[tblAdditionalReq cellForRowAtIndexPath: [NSIndexPath indexPathForRow:[sender tag] inSection:0] ];
    
    if ([additinalReq.btnCheckbox isSelected])
        [additinalReq.btnCheckbox setSelected:NO];
    else if (![additinalReq.btnCheckbox isSelected])
        [additinalReq.btnCheckbox setSelected:YES];
    
    //to check if all the requests have been accepted
    for (int i = 0 ; i<additionalRequests.count; i++) {
        HXAdditionalReqCell *additinalReq = (HXAdditionalReqCell *)[tblAdditionalReq cellForRowAtIndexPath: [NSIndexPath indexPathForRow:i inSection:0] ];
        if ([additinalReq.btnCheckbox isSelected]) {
            allTicked = YES;
        }
        else
        {
            allTicked = NO;
            break;
        }
    }
    if (allTicked)
    {
        [_btnAccept setEnabled:YES];
    }
}

//Temp code for flow
-(void)retriveCurrentStatus
{
    NSUserDefaults *currentJob = [NSUserDefaults standardUserDefaults];
    currentStatus = [currentJob stringForKey:@"jobStatus"];
}

-(void)loadUI
{
    if ([currentStatus isEqualToString:@"na"]) {
        [_btnAccept setHidden:NO];
    }
    else
    {
        [_btnAccept setHidden:YES];
    }
}


#pragma mark - Table View delegate and DataSource


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    id tableViewCell;
    if (tableView == tblJobDetails)
    {
        static NSString *CellIdentifier = @"cellType1";
        
        HXTableCellType1* cell = (HXTableCellType1 *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        cell.lblTitle.text = _titleList[indexPath.row];
        
        if ([_navigationStatus[indexPath.row] isEqualToString:@"P"])
        {
            [cell.imgDetails setHidden:NO];
            [cell.imgDetails setHighlighted:YES];
        }
        else if ([_navigationStatus[indexPath.row] isEqualToString:@"Y"])
        {
            [cell.imgDetails setHidden:NO];
            [cell.imgDetails setHighlighted:NO];
        }
        else
        {
            [cell.imgDetails setHidden:YES];
        }
        tableViewCell = cell;
        
        switch (indexPath.row) {
            case EJOBID:
                cell.lblContent.text = jobdetail.bookingNumber;
                break;
            case EPUTIME:
                cell.lblContent.text = jobdetail.pickupTime;
                break;
            case EACCOUNT:
                cell.lblContent.text = jobdetail.accountName;
                break;
            case EPASSENGER:
                cell.lblContent.text = pasengerNameDisplay;
                break;
            case EPUADDRESS:
                cell.lblContent.text = jobdetail.puSuburb;
                break;
            case EDRADDRESS:
                cell.lblContent.text = jobdetail.drAddress;
                break;
            case ESPLINSTRUCTION:
                cell.lblContent.text = jobdetail.specialInstructions;
                break;
            case EVEHICLETYPE:
                cell.lblContent.text = jobdetail.refVehicleType;
                break;
            case ELEGS:
                cell.lblContent.text = [NSString stringWithFormat:@"%i",[HXMainModel sharedInstance].additionalJobDetailsModel.legList.count];
                break;
        }
        
        
    }
    else if (tableView == tblAdditionalReq)
    {
        static NSString *CellIdentifier = @"HXAdditionalReqCell";
        HXAdditionalReqCell *additinalReq = (HXAdditionalReqCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
        if (respectiveAdditionalReqStringList.count>0) 
        //populate data from your additional request object to table view cell
        additinalReq.lblRequest.text =[respectiveAdditionalReqStringList objectAtIndex:indexPath.row];
        [additinalReq.btnCheckbox setTag:indexPath.row];
        [additinalReq.btnCheckbox addTarget:self action:@selector(checkboxTicked:) forControlEvents:UIControlEventTouchUpInside];
        tableViewCell = additinalReq;
    }
    return tableViewCell;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int count;
    if (tableView == tblJobDetails) {

        count =  _titleList.count;
    }
    else if (tableView == tblAdditionalReq)
    {
        count = additionalRequests.count;
    }
    return count;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == tblJobDetails) {

        switch (indexPath.row) {
            case 3:
            {
                NSMutableString *passengerListString = [[NSMutableString alloc]init];
                if (jobdetail.passengersList.count == 0) {
                    [passengerListString appendString:@""];
                }
                for (NSString *passenger in jobdetail.passengersList) {
                    [passengerListString appendString:[NSString stringWithFormat:@"%@\n", passenger]];
                }
                 
            }
            break;
                
            case 4:
                [self createAlertWithTitle:@"Pick-Up Address" andMessage:jobdetail.puAddress];
                break;
                
            case 5:
                [self createAlertWithTitle:@"Drop Address" andMessage:jobdetail.drAddress];
                break;
            case 6:
                [self createAlertWithTitle:@"Special Instructions" andMessage:jobdetail.specialInstructions];
                break;
                
            case 8:
            {
                HXJobLegsVC *legsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"JobLegs"];
                [self.navigationController pushViewController:legsVC animated:YES];
            }
                break;
                
                
            default:
                break;
        }

    }
}

#pragma mark - Action Implementation

- (IBAction)btnBackAction:(id)sender {
    if (additionalReqViewVisible) {
        [UIView animateWithDuration:0.25 animations:^{
            [viewAdditionalReq setFrame:CGRectMake(viewAdditionalReq.frame.origin.x
                                                   , -448, viewAdditionalReq.frame.size.width, viewAdditionalReq.frame.size.height)];
        }];
        
        _navBarItem.title = @"Job Details";
        additionalReqViewVisible = NO;
        [_btnAccept setEnabled:YES];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}
- (IBAction)btnAcceptAction:(id)sender {
    
    if (respectiveAdditionalReqStringList.count > 0) {//ADDITIONAL REQUESTS AVAILABLE, DISPLAY VIEW
        if (additionalReqViewVisible && allTicked)
        {
            NSUserDefaults *currentJob = [NSUserDefaults standardUserDefaults];
            [currentJob setObject:@"Accepted" forKey:@"jobStatus"];
            
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            if (!additionalReqViewVisible ) {
                [UIView animateWithDuration:0.25 animations:^{
                    [viewAdditionalReq setFrame:CGRectMake(viewAdditionalReq.frame.origin.x
                                                           , 64, viewAdditionalReq.frame.size.width, viewAdditionalReq.frame.size.height)];
                }];
                additionalReqViewVisible = YES;
                
                _navBarItem.title = @"Additional Request";
                if (!allTicked) {
                    [_btnAccept setEnabled:NO];
                }
            }
        }

    }
        else // as additional requests not available, dont display the view
        {
            NSUserDefaults *currentJob = [NSUserDefaults standardUserDefaults];
            [currentJob setObject:@"Accepted" forKey:@"jobStatus"];
            
            [self.navigationController popViewControllerAnimated:YES];

        }

    
}
/*
- (IBAction)btnAcceptAction:(id)sender {
   
    if (additionalReqViewVisible && allTicked)
    {
        NSUserDefaults *currentJob = [NSUserDefaults standardUserDefaults];
        [currentJob setObject:@"Accepted" forKey:@"jobStatus"];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        if (!additionalReqViewVisible && respectiveAdditionalReqStringList.count>0) {//chechking if we have any additional requests else not to display additional request view
            [UIView animateWithDuration:0.25 animations:^{
                [viewAdditionalReq setFrame:CGRectMake(viewAdditionalReq.frame.origin.x
                                                       , 64, viewAdditionalReq.frame.size.width, viewAdditionalReq.frame.size.height)];
            }];
            additionalReqViewVisible = YES;
            
            _navBarItem.title = @"Additional Request";
            if (!allTicked) {
                [_btnAccept setEnabled:NO];
            }
        }
    }

}
 */
@end
