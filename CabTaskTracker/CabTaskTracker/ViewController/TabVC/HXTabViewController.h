//
//  HXTabViewController.h
//  CabTaskTracker
//
//  Created by Mithun R on 02/07/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HXTabViewController : UITabBarController
@property (strong, nonatomic) IBOutlet UITabBar *tabBarJobs;

@end
