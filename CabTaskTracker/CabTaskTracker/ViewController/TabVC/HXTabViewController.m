//
//  HXTabViewController.m
//  CabTaskTracker
//
//  Created by Mithun R on 02/07/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import "HXTabViewController.h"

#import "HXMainModel.h"

@interface HXTabViewController ()
{
    NSString *currentStatus;
}
@end

@implementation HXTabViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[HXMainModel sharedInstance] fetchPredefinedListsFromMaintenanceDB];
    
    //TO CHANGE THE TABBAR ITEM TITLE FONT SIZE
//    [[[_tabBarJobs items]objectAtIndex:0]setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:                                                                         [UIFont systemFontOfSize:17.0f], NSFontAttributeName, nil]
//                                                       forState:UIControlStateNormal];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self retriveCurrentStatus];

    [self loadUI];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - method Implementation

-(void)retriveCurrentStatus
{
    NSUserDefaults *currentJob = [NSUserDefaults standardUserDefaults];
    currentStatus = [currentJob stringForKey:@"jobStatus"];
}

-(void)loadUI
{
    if ([currentStatus isEqualToString:@"na"] || [currentStatus isEqualToString:@"Completed"]) {
        
        [[[_tabBarJobs items]objectAtIndex:1]setEnabled:NO];
        [self setSelectedIndex:0];
    }
    else
    {
        [[[_tabBarJobs items]objectAtIndex:1]setEnabled:YES];
        [self setSelectedIndex:1];
    }
}
@end
