//
//  HXTableCellType1.h
//  CabTaskTracker
//
//  Created by Mithun R on 02/07/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HXTableCellType1 : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblContent;
@property (strong, nonatomic) IBOutlet UIImageView *imgDetails;

@end
