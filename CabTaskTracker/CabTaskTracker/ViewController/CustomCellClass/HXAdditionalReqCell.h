//
//  HXAdditionalReqCell.h
//  CabTaskTracker
//
//  Created by aditi on 04/07/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HXAdditionalReqCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIButton *btnCheckbox;
@property (strong, nonatomic) IBOutlet UILabel *lblRequest;
@property BOOL chechkboxTicked;
@end
