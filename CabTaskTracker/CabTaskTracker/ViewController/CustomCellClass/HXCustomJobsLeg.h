//
//  HXCustomJobsLeg.h
//  WireFrames
//
//  Created by Mithun R on 27/06/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HXCustomJobsLeg : UITableViewCell

@property(strong,nonatomic)IBOutlet UILabel *lblLegNo;
@property(strong,nonatomic)IBOutlet UILabel *lblInfoStreet;
@property(strong,nonatomic)IBOutlet UILabel *lblSubuab;
@property(strong,nonatomic)IBOutlet UILabel *lblMobileNumber;
@property(strong,nonatomic)IBOutlet UIButton *btnCallbutton;


//-(IBAction)btnCallAction:(id)sender;
@end
