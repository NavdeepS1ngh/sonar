//
//  HXExtrasCell.m
//  CabTaskTracker
//
//  Created by Mithun R on 04/07/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import "HXExtrasCell.h"

#define TBLENGTH 6;

enum errorCode
{
    EONLYNUM = 0,
    EMORETHAN99,
    ESINGLEDOT,
};

@implementation HXExtrasCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)createRegularExpression
{
    NSString *pattern;
    NSError  *error = nil;
    
    //For Num check
    pattern = @"[0-9]+";
    _numRegEx = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:&error];
    
    //For Dot Check
    pattern = @"\\.";
    _dotRegEx = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:&error];
    
    //For NumWithDot Check
    pattern = @"[0-9]*\\.";
    _numDotRegEx = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:&error];
}

#pragma mark - Action Implementation
- (IBAction)tbCellEditAction:(id)sender {
    if (_tbInputValue.text.floatValue > 99) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"Value is greater than $99" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
    }
}

#pragma mark - TextField Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self createRegularExpression];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //[textField2 setText:[textField1.text stringByReplacingCharactersInRange:range withString:string]];
    
    NSRange   searchedRange = NSMakeRange(0, [string length]);
    NSRange dotNumRange = NSMakeRange(0, [textField.text length]);
    
    NSTextCheckingResult *matchNum = [_numRegEx firstMatchInString:string  options:0 range: searchedRange];
    NSTextCheckingResult *matchDot = [_dotRegEx firstMatchInString:string  options:0 range: searchedRange];
    NSTextCheckingResult *matchNumDot = [_numDotRegEx firstMatchInString:textField.text options:0 range: dotNumRange];
    
    if (matchNum) {
        return YES;
    }
    else if (matchDot)
    {
        if (matchNumDot)
            return NO;
        else
            return YES;
    }
    else if ([string isEqualToString:@""])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}
@end
