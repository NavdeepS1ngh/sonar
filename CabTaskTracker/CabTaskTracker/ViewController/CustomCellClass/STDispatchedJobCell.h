//
//  STAllJobsCellTableViewCell.h
//  ETG_App
//
//  Created by aditi on 02/07/14.
//  Copyright (c) 2014 hexaware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface STDispatchedJobCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblJobId;
@property (strong, nonatomic) IBOutlet UILabel *lblPickupTime;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblPickupAddress;
@property (strong, nonatomic) IBOutlet UIImageView *imgNavigateIcon;


//-(NSArray *)fetchAllSundriesListFromDB;

@end
