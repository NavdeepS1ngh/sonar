//
//  HXExtrasCell.h
//  CabTaskTracker
//
//  Created by Mithun R on 04/07/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HXExtrasCell : UITableViewCell <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *tbInputValue;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;

@property (strong, nonatomic) NSRegularExpression *numRegEx;
@property (strong, nonatomic) NSRegularExpression *dotRegEx;
@property (strong, nonatomic) NSRegularExpression *numDotRegEx;

//@property (strong, nonatomic) NSRegularExpression *numGroupRegEx;

- (IBAction)tbCellEditAction:(id)sender;

@end
