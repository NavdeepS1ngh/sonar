//
//  STDispatchedJobCell.h
//  ETG_App
//
//  Created by aditi on 02/07/14.
//  Copyright (c) 2014 hexaware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface STAllJobCellTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblPickupTime;
@property (strong, nonatomic) IBOutlet UILabel *lblPickupPersonName;
@property (strong, nonatomic) IBOutlet UILabel *lblPickupAddress;

@end
