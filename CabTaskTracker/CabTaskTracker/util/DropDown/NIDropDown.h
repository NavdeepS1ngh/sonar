//
//  NIDropDown.h
//  NIDropDown

#import <UIKit/UIKit.h>

@class NIDropDown;
@protocol NIDropDownDelegate
- (void) niDropDownDelegateMethod: (NIDropDown *) sender selectedRowId:(NSString*)id;
@end

@interface NIDropDown : UIView <UITableViewDelegate, UITableViewDataSource>
{
    NSString *animationDirection;
    UIImageView *imgView;
}
@property (nonatomic, retain) id <NIDropDownDelegate> delegate;
@property (nonatomic, retain) NSString *animationDirection;
-(void)hideDropDown:(UIButton *)b;
- (id)showDropDown:(UIButton *)bttn withHeight:(CGFloat *)height withArray:(NSArray *)arr withImgArray:(NSArray *)imgArr andDirection:(NSString *)direction;
@end
