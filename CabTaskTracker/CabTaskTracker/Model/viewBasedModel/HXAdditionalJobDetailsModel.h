//
//  HXAdditionalJobDetailsModel.h
//  CabTaskTracker
//
//  Created by Mithun on 11/08/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import <Foundation/Foundation.h>


//Specific for individual booking, need to refresh for different booking:
@interface HXAdditionalJobDetailsModel : NSObject


//Note: To be set once the class is initiated, else it will throw error:
@property (nonatomic, strong) NSString *refBookingID;

@property (nonatomic, strong) NSMutableArray *additionalRequestList;
@property (nonatomic, strong) NSMutableArray  *legList;

-(void)loadAdditionalData;

@end
