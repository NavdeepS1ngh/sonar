//
//  HXJobDetailsModel.m
//  CabTaskTracker
//
//  Created by Mithun on 07/08/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import "HXJobDetailsModel.h"
#import "HXCoreDataManager.h"

#import "HXMainModel.h"

@implementation HXJobDetailsModel


-(void)loadJobDetailsFromDB
{
    [HXMainModel sharedInstance].jobDetailsList = [[NSMutableArray alloc]init];
 
    
    NSArray* results = [[HXCoreDataManager sharedInstance]getMatchedRowsFromEntity:@"Bookings" predicateString:@""];
    
    if (results.count > 0) {
        for (NSManagedObject* obj in results) {
            HXJobDetailsModel* jobDetail = [[HXJobDetailsModel alloc]init];
            
            jobDetail.bookingID = [obj valueForKey:@"bookingID"];
            jobDetail.bookingNumber = [obj valueForKey:@"bookingNumber"];
            jobDetail.isDispatched = [obj valueForKey:@"isDispatched"];
            jobDetail.refVehicleType = [obj valueForKey:@"refVehicleType"];
            jobDetail.pickupTime = [obj valueForKey:@"pickupTime"];
            jobDetail.puSuburb = [obj valueForKey:@"puSuburb"];
            jobDetail.puAddress = [obj valueForKey:@"puAddress"];
            jobDetail.refChauffeur = [obj valueForKey:@"refChauffeur"];
            jobDetail.drSuburb = [obj valueForKey:@"drSuburb"];
            jobDetail.drAddress = [obj valueForKey:@"drAddress"];
            jobDetail.specialInstructions = [obj valueForKey:@"specialInstructions"];
            jobDetail.accountName = [obj valueForKey:@"accountName"];
            jobDetail.allowSundries = [obj valueForKey:@"allowSundries"];
            jobDetail.allowKm = [obj valueForKey:@"allowKm"];
            jobDetail.totalKM = [obj valueForKey:@"totalKM"];
            jobDetail.waitingTime = [obj valueForKey:@"waitingTime"];


            ///for passengers of a particular booking
                       NSString *predicateString = [NSString stringWithFormat:@"refBooking==%@", jobDetail.bookingID];
            
             NSArray* passengers = [[HXCoreDataManager sharedInstance]getMatchedRowsFromEntity:@"Booking_Passenger" predicateString:predicateString];
            jobDetail.passengersList = [[NSMutableArray alloc]init];
            for (NSManagedObject* obj in passengers) {
                NSString* name =[NSString stringWithFormat:@"%@ %@",[obj valueForKey:@"firstName"],[obj valueForKey:@"lastName"]];
                [jobDetail.passengersList addObject:name];
            }
            
            ////////
            [[HXMainModel sharedInstance].jobDetailsList addObject:jobDetail];
            
        }
        [[[HXCoreDataManager sharedInstance] managedObjectContext] save:nil];
    }
    else
    {
        NSLog(@"No bookings available!");
    }

}

@end
