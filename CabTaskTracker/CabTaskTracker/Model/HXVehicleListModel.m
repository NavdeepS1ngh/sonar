//
//  HXVehicleListModel.m
//  CabTaskTracker
//
//  Created by Mithun on 30/07/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import "HXVehicleListModel.h"

#import "HXMainModel.h"
#import "HXWebServiceManager.h"

@implementation HXVehicleListModel
{
    HXWebServiceManager *serviceManager;

}

-(id)init
{
    if ((self = [super init]))
    {
        serviceManager = [[HXWebServiceManager alloc]init];
        
    }
    return self;
}

-(void)getVehicleList
{
    [serviceManager setServiceDelegate:self];
    [serviceManager getVehicleListForUserName:[HXMainModel sharedInstance].currentUser];
}




//TODO: Protocal Implementation:
-(void)recievedVehicleList:(NSArray *)vehicleList
{
    //id key = [[vehicleList allKeys] objectAtIndex:0]; // Assumes 'message' is not empty
    //NSArray *recievedListAry = [vehicleList allValues];//[key objectForKey:key];
    _vehicleListAry = [[NSMutableArray alloc]init];
    
    for (NSDictionary *vehicleDetails in vehicleList)
    {
        _vehicleModel = [[HXVehicleListModel alloc]init];
        
        _vehicleModel.vehicleID = [NSString stringWithFormat:@"%@",[vehicleDetails valueForKey:@"id"]];
        _vehicleModel.chauffeurID = [[HXMainModel sharedInstance]currentUser];
        _vehicleModel.rego = [vehicleDetails valueForKey:@"rego"];
        _vehicleModel.refVehicleType = [NSString stringWithFormat:@"%@",[vehicleDetails valueForKey:@"refVehicleType"]];
        _vehicleModel.isVehicleActive = [NSString stringWithFormat:@"%@",[vehicleDetails valueForKey:@"isActive"]];
        
        [_vehicleListAry addObject:_vehicleModel];
                                         
    }
    
    
    NSDate *currDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"dd.MM.YY HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:currDate];
    
    _lastUpdateTime = dateString;
    _messageFromServer = @"OK";
    
    [_modelDelegate storeVehicleListInDBWithMessage:_messageFromServer];
}

-(void)recievedVehicleListFailedWithReason:(NSString *)reason
{
    _messageFromServer = reason;
    [_modelDelegate storeVehicleListInDBWithMessage:reason];
    //Send the reason back and display in view:
}


@end
