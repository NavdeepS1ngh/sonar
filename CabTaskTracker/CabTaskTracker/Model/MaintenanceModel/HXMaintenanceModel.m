//
//  HXMaintenanceModel.m
//  CabTaskTracker
//
//  Created by Mithun on 05/08/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import "HXMaintenanceModel.h"

#import "HXMainModel.h"
#import "HXWebServiceManager.h"
#import "HXCoreDataManager.h"

@implementation HXMaintenanceUtilModel

@end

#import "HXMaintRequestModel.h"
#import "HXMaintSundriesModel.h"
#import "HXMaintBookingStatusModel.h"
#import "HXMaintVehicleTypeModel.h"
#import "HXMaintChauffeurStatusModel.h"

@implementation HXMaintenanceModel
{
    HXWebServiceManager *serviceManager;
    
    NSManagedObjectContext *context;
    NSEntityDescription *entityDesc;
    HXCoreDataManager* coreDataManager;
    
    HXMaintenanceUtilModel *utilObject;
}

-(void)getMaintenanceData
{
    serviceManager = [[HXWebServiceManager alloc]init];
    [serviceManager setServiceDelegate:(HXMaintenanceModel *)self];
    [serviceManager getMaintenanceRequest];
}

-(NSArray *)fetchAllRequestListFromDB
{
    NSMutableArray *allRequestList = [[NSMutableArray alloc]init];
    
    NSArray* results = [[HXCoreDataManager sharedInstance] getMatchedRowsFromEntity:@"Maintenance_Request" predicateString:@""];
    for (NSManagedObject* obj in results) {
        
        utilObject = [[HXMaintenanceUtilModel alloc]init];
        utilObject.utilID = [obj valueForKey:@"requestID"];
        utilObject.utilDescription = [obj valueForKey:@"requestDescription"];
        utilObject.utilIsActive = [obj valueForKey:@"isActive"];
        
        [allRequestList addObject:utilObject];

    }
    [[HXCoreDataManager sharedInstance]saveContext];
    return (NSArray *)allRequestList;
    
}
-(NSArray *)fetchAllJobStatusListFromDB
{
    NSMutableArray *allJobStatusList = [[NSMutableArray alloc]init];
    
    NSArray* results = [[HXCoreDataManager sharedInstance] getMatchedRowsFromEntity:@"Maintenance_BookingStatus" predicateString:@""];
    for (NSManagedObject* obj in results) {
        
        utilObject = [[HXMaintenanceUtilModel alloc]init];
        utilObject.utilID = [obj valueForKey:@"bookingStatusID"];
        utilObject.utilDescription = [obj valueForKey:@"bookingStatusDescription"];
        utilObject.utilIsActive = [obj valueForKey:@"isActive"];
        
        [allJobStatusList addObject:utilObject];
        
    }
    [[HXCoreDataManager sharedInstance]saveContext];
    return (NSArray *)allJobStatusList;

}
-(NSArray *)fetchAllSundriesListFromDB
{
    NSMutableArray *allSundriesList = [[NSMutableArray alloc]init];
    
    NSArray* results = [[HXCoreDataManager sharedInstance] getMatchedRowsFromEntity:@"Maintenance_Sundry" predicateString:@""];
    for (NSManagedObject* obj in results) {
        
        utilObject = [[HXMaintenanceUtilModel alloc]init];
        utilObject.utilID = [obj valueForKey:@"sundryID"];
        utilObject.utilDescription = [obj valueForKey:@"sundryDescription"];
        utilObject.utilIsActive = [obj valueForKey:@"isActive"];
        
        [allSundriesList addObject:utilObject];
        
    }
    [[HXCoreDataManager sharedInstance]saveContext];
    return (NSArray *)allSundriesList;
}
-(NSArray *)fetchAllVehicleListFromDB
{
    NSMutableArray *allvehicleList = [[NSMutableArray alloc]init];
    
    NSArray* results = [[HXCoreDataManager sharedInstance] getMatchedRowsFromEntity:@"Maintenance_VehicleType" predicateString:@""];
    for (NSManagedObject* obj in results) {
        
        utilObject = [[HXMaintenanceUtilModel alloc]init];
        utilObject.utilID = [obj valueForKey:@"vehicleID"];
        utilObject.utilDescription = [obj valueForKey:@"vehicleDescription"];
        utilObject.utilIsActive = [obj valueForKey:@"isActive"];
        
        [allvehicleList addObject:utilObject];
        
    }
    [[HXCoreDataManager sharedInstance]saveContext];
    return (NSArray *)allvehicleList;
}

-(void)storeRecieviedDataInDB
{
    //Store the available data in model to DB:
    
    coreDataManager = [HXCoreDataManager sharedInstance];
    context = [coreDataManager managedObjectContext];
    
    for (HXMaintBookingStatusModel* obj in self.bookingStatusArray) {
        
        NSString *predicateString = [NSString stringWithFormat:@"bookingStatusID==\"%@\"", obj.utilID];
        
        NSArray* results = [[HXCoreDataManager sharedInstance]getMatchedRowsFromEntity:@"Maintenance_BookingStatus" predicateString:predicateString];
        
        if (results.count > 0) {
            //updation
            NSManagedObject* tblRow = [results objectAtIndex:0 ];
            
            [tblRow setValue:obj.utilDescription forKey:@"bookingStatusDescription"];
            [tblRow setValue:obj.utilID forKey:@"bookingStatusID"];
            [tblRow setValue:obj.utilIsActive forKey:@"isActive"];
            
            [context save:nil];

        }
        else{
        NSManagedObject* tblRow = [NSEntityDescription insertNewObjectForEntityForName:@"Maintenance_BookingStatus" inManagedObjectContext:context];
        
        [tblRow setValue:obj.utilDescription forKey:@"bookingStatusDescription"];
        [tblRow setValue:obj.utilID forKey:@"bookingStatusID"];
        [tblRow setValue:obj.utilIsActive forKey:@"isActive"];
        
        [context save:nil];
        }
    }
    
    for (HXMaintRequestModel* obj in self.requestArray) {
        NSString *predicateString = [NSString stringWithFormat:@"requestID==\"%@\"", obj.utilID];
        
        NSArray* results = [[HXCoreDataManager sharedInstance]getMatchedRowsFromEntity:@"Maintenance_Request" predicateString:predicateString];
        
        if (results.count > 0) {
            //updation
            NSManagedObject* tblRow = [results objectAtIndex:0 ];
            
            [tblRow setValue:obj.utilDescription forKey:@"requestDescription"];
            [tblRow setValue:obj.utilID forKey:@"requestID"];
            [tblRow setValue:obj.utilIsActive forKey:@"isActive"];
            
            [context save:nil];
        }
        else
        {
        NSManagedObject* tblRow = [NSEntityDescription insertNewObjectForEntityForName:@"Maintenance_Request" inManagedObjectContext:context];
        
        [tblRow setValue:obj.utilDescription forKey:@"requestDescription"];
        [tblRow setValue:obj.utilID forKey:@"requestID"];
        [tblRow setValue:obj.utilIsActive forKey:@"isActive"];
        
        [context save:nil];
        }
    }
    
    for (HXMaintSundriesModel* obj in self.sundriesArray) {
        NSString *predicateString = [NSString stringWithFormat:@"sundryID==\"%@\"", obj.utilID];
        
        NSArray* results = [[HXCoreDataManager sharedInstance]getMatchedRowsFromEntity:@"Maintenance_Sundry" predicateString:predicateString];
        
        if (results.count > 0) {
            //updation
            NSManagedObject* tblRow = [results objectAtIndex:0 ];
            [tblRow setValue:obj.utilDescription forKey:@"sundryDescription"];
            [tblRow setValue:obj.utilID forKey:@"sundryID"];
            [tblRow setValue:obj.utilIsActive forKey:@"isActive"];
            
            [context save:nil];

        }
        else
        {

        NSManagedObject* tblRow = [NSEntityDescription insertNewObjectForEntityForName:@"Maintenance_Sundry" inManagedObjectContext:context];
        
        [tblRow setValue:obj.utilDescription forKey:@"sundryDescription"];
        [tblRow setValue:obj.utilID forKey:@"sundryID"];
        [tblRow setValue:obj.utilIsActive forKey:@"isActive"];
        
        [context save:nil];
            
        }
    }
    
    for (HXMaintVehicleTypeModel* obj in self.vehicleTypeArray) {
        NSString *predicateString = [NSString stringWithFormat:@"vehicleID==\"%@\"", obj.utilID];
        
        NSArray* results = [[HXCoreDataManager sharedInstance]getMatchedRowsFromEntity:@"Maintenance_VehicleType" predicateString:predicateString];
        
        if (results.count > 0) {
            //updation
            NSManagedObject* tblRow = [results objectAtIndex:0 ];
            
            [tblRow setValue:obj.utilDescription forKey:@"vehicleDescription"];
            [tblRow setValue:obj.utilID forKey:@"vehicleID"];
            [tblRow setValue:obj.utilCode forKey:@"vehicleCode"];
            [tblRow setValue:obj.utilIsActive forKey:@"isActive"];
            
            [context save:nil];

        }
        else{
        NSManagedObject* tblRow = [NSEntityDescription insertNewObjectForEntityForName:@"Maintenance_VehicleType" inManagedObjectContext:context];
        
        [tblRow setValue:obj.utilDescription forKey:@"vehicleDescription"];
        [tblRow setValue:obj.utilID forKey:@"vehicleID"];
        [tblRow setValue:obj.utilCode forKey:@"vehicleCode"];
        [tblRow setValue:obj.utilIsActive forKey:@"isActive"];
        
        [context save:nil];
        }
    }
    
    for (HXMaintChauffeurStatusModel* obj in self.chauffeurStatusArray) {
        NSString *predicateString = [NSString stringWithFormat:@"statusID==\"%@\"", obj.utilID];
        
        NSArray* results = [[HXCoreDataManager sharedInstance]getMatchedRowsFromEntity:@"Maintenance_ChaufferStatus" predicateString:predicateString];
        
        if (results.count > 0) {
            //updation
            NSManagedObject* tblRow = [results objectAtIndex:0 ];
            
            [tblRow setValue:obj.utilDescription forKey:@"statusDescription"];
            [tblRow setValue:obj.utilID forKey:@"statusID"];
            [tblRow setValue:obj.utilIsActive forKey:@"isActive"];
            
            [context save:nil];

        }
        else{
        NSManagedObject* tblRow = [NSEntityDescription insertNewObjectForEntityForName:@"Maintenance_ChaufferStatus" inManagedObjectContext:context];
        
        [tblRow setValue:obj.utilDescription forKey:@"statusDescription"];
        [tblRow setValue:obj.utilID forKey:@"statusID"];
        [tblRow setValue:obj.utilIsActive forKey:@"isActive"];
        
        [context save:nil];
        }
    }
    
    NSLog(@"Storing SyncMaint in DB completed");
    
    
    //Call protocol HXModelDelegate to inform fetch is complete:
    [_modelDelegate storeMaintenanceList_Status:@"OK"];
    
}

-(void)retriveDataToModelFromDB
{
    //retrieve data to model once records have been updated in model:
    
    //TO be called in offline login mode and after update in DB from webAPI
}

#pragma mark - Protocol Implementation: HXWebServiceProtocol
-(void)recievedMaintenanceData:(NSDictionary *)maintenanceDict
{
    
    NSDictionary *chaufferDict = [[NSDictionary alloc]initWithDictionary:[maintenanceDict objectForKey:@"chauffeur"]];
    
        [self setChauffeurID:[chaufferDict valueForKey:@"id"]];
        [self setLicenseExpiryDate:[chaufferDict valueForKey:@"licenseExpiryDate"]];
        [self setChaufferIsActive:[NSString stringWithFormat:@"%@",[chaufferDict valueForKey:@"isActive"]]];
    
    NSArray *recievedConfigurationArray = [maintenanceDict objectForKey:@"configuration"];
        for (NSDictionary *tempDict in recievedConfigurationArray) {
            
            NSString *configType = [tempDict valueForKey:@"name"];
            
            if ([configType isEqualToString:@"LICENSEWARNINGPERIOD"]) {
                [self setLicenceWarningPeriod:[tempDict valueForKey:@"value"]];
            }
            else if ([configType isEqualToString:@"GPSFREQUENCY"]) {
                [self setGpsFrequency:[tempDict valueForKey:@"value"]];
            }
            else if ([configType isEqualToString:@"MESSAGEFREQUENCY"]) {
                [self setMessageFrequency:[tempDict valueForKey:@"value"]];
            }
            else if ([configType isEqualToString:@"OPSFREQUENCY"]) {
                [self setOpsFrequency:[tempDict valueForKey:@"value"]];
            }
            else if ([configType isEqualToString:@"TEST"]) {
                //[self asd:[tempDict valueForKey:@"value"]];
            }
        }
    
    NSArray *recievedRequestArray = [maintenanceDict objectForKey:@"requests"];
    _requestArray = [[NSMutableArray alloc]init];
    for (NSDictionary *tempDict in recievedRequestArray) {
        
        _requestModel = [[HXMaintRequestModel alloc]init];
        
        [_requestModel setUtilID: [NSString stringWithFormat:@"%@",[tempDict valueForKey:@"id"]]];
        [_requestModel setUtilIsActive: [NSString stringWithFormat:@"%@",[tempDict valueForKey:@"isActive"]]];
        [_requestModel setUtilDescription: [NSString stringWithFormat:@"%@",[tempDict valueForKey:@"description"]]];
        
        [_requestArray addObject:_requestModel];
    }
    
    NSArray *recievedSundriesArray = [maintenanceDict objectForKey:@"sundries"];
    _sundriesArray = [[NSMutableArray alloc]init];
    for (NSDictionary *tempDict in recievedSundriesArray) {
        
        _sundryModel  = [[HXMaintSundriesModel alloc]init];
        
        [_sundryModel setUtilID: [NSString stringWithFormat:@"%@",[tempDict valueForKey:@"id"]]];
        [_sundryModel setUtilIsActive: [NSString stringWithFormat:@"%@",[tempDict valueForKey:@"isActive"]]];
        [_sundryModel setUtilDescription: [NSString stringWithFormat:@"%@",[tempDict valueForKey:@"description"]]];
        
        [_sundriesArray addObject:_sundryModel];
    }
     
    
    NSArray *recievedBookingStatusArray = [maintenanceDict objectForKey:@"bookingStatuses"];
    _bookingStatusArray = [[NSMutableArray alloc]init];
    for (NSDictionary *tempDict in recievedBookingStatusArray) {
        
        _bookingStatusModel = [[HXMaintBookingStatusModel alloc]init];
        
        [_bookingStatusModel setUtilID: [NSString stringWithFormat:@"%@",[tempDict valueForKey:@"id"]]];
        [_bookingStatusModel setUtilIsActive: [NSString stringWithFormat:@"%@",[tempDict valueForKey:@"isActive"]]];
        [_bookingStatusModel setUtilDescription: [NSString stringWithFormat:@"%@",[tempDict valueForKey:@"description"]]];
        
        [_bookingStatusArray addObject:_bookingStatusModel];
    }
    
    NSArray *recievedVehicleTypes = [maintenanceDict objectForKey:@"vehicleTypes"];
    _vehicleTypeArray = [[NSMutableArray alloc]init];
    for (NSDictionary *tempDict in recievedVehicleTypes) {
        
        _vehicleTypeModel = [[HXMaintVehicleTypeModel alloc]init];
        
        [_vehicleTypeModel setUtilCode:[tempDict valueForKey:@"code"]];
        [_vehicleTypeModel setUtilID: [NSString stringWithFormat:@"%@",[tempDict valueForKey:@"id"]]];
        [_vehicleTypeModel setUtilIsActive: [NSString stringWithFormat:@"%@",[tempDict valueForKey:@"isActive"]]];
        [_vehicleTypeModel setUtilDescription: [NSString stringWithFormat:@"%@",[tempDict valueForKey:@"description"]]];
        
        [_vehicleTypeArray addObject:_vehicleTypeModel];
    }
    
    NSArray *recievedChauffeurStatusArray = [maintenanceDict objectForKey:@"chauffeurStatuses"];
    _chauffeurStatusArray = [[NSMutableArray alloc]init];
    for (NSDictionary *tempDict in recievedChauffeurStatusArray) {
        
        _chaufferStatusArray = [[HXMaintChauffeurStatusModel alloc]init];
        
        [_chaufferStatusArray setUtilID: [NSString stringWithFormat:@"%@",[tempDict valueForKey:@"id"]]];
        [_chaufferStatusArray setUtilIsActive: [NSString stringWithFormat:@"%@",[tempDict valueForKey:@"isActive"]]];
        [_chaufferStatusArray setUtilDescription: [NSString stringWithFormat:@"%@",[tempDict valueForKey:@"description"]]];
        
        [_chauffeurStatusArray addObject:_chaufferStatusArray];
    }
    [HXMainModel saveSyncMaintenanceKey:[maintenanceDict valueForKey:@"syncDate"]];
    
    
    NSLog(@"get Maintenance Completed, about to store in DB");
    [self storeRecieviedDataInDB];
    
}
-(void)recievedMaintenanceFailedWithReason:(NSString *)reason
{
    NSLog(@"Maintenance recieved failure");
    [_modelDelegate storeMaintenanceList_Status:reason];
}


@end
