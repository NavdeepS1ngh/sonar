//
//  HXMaintVehicleTypeModel.h
//  CabTaskTracker
//
//  Created by Mithun on 06/08/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import "HXMaintenanceModel.h"

@interface HXMaintVehicleTypeModel : HXMaintenanceUtilModel
@property (nonatomic, strong) NSString *utilCode;
@end
