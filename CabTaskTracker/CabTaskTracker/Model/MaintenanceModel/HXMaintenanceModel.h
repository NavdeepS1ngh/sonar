//
//  HXMaintenanceModel.h
//  CabTaskTracker
//
//  Created by Mithun on 05/08/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "HXServiceMgrProtocol.h"
#import "HXModelMgrProtocol.h"


@interface HXMaintenanceUtilModel : NSObject

@property (nonatomic, strong) NSString *utilID;
@property (nonatomic, strong) NSString *utilDescription;
@property (nonatomic, strong) NSString *utilIsActive;

@end

@class HXMaintRequestModel;
@class HXMaintSundriesModel;
@class HXMaintBookingStatusModel;
@class HXMaintVehicleTypeModel;
@class HXMaintChauffeurStatusModel;

@interface HXMaintenanceModel : NSObject <HXServiceMgrProtocol>

@property(readwrite,assign)id<HXModelMgrProtocol>modelDelegate;

//for chauffer Details
@property (nonatomic, strong) NSString *chauffeurID;
@property (nonatomic, strong) NSString *licenseExpiryDate;
@property (nonatomic, strong) NSString *chaufferIsActive;

//for congig table
@property (nonatomic, strong) NSString *licenceWarningPeriod;
@property (nonatomic, strong) NSString *gpsFrequency;
@property (nonatomic, strong) NSString *messageFrequency;
@property (nonatomic, strong) NSString *opsFrequency;


@property (nonatomic, strong) NSMutableArray *requestArray;
@property (nonatomic, strong) NSMutableArray *sundriesArray;
@property (nonatomic, strong) NSMutableArray *bookingStatusArray;
@property (nonatomic, strong) NSMutableArray *vehicleTypeArray;
@property (nonatomic, strong) NSMutableArray *chauffeurStatusArray;

@property (nonatomic, strong) HXMaintRequestModel *requestModel;
@property (nonatomic, strong) HXMaintSundriesModel *sundryModel;
@property (nonatomic, strong) HXMaintBookingStatusModel *bookingStatusModel;
@property (nonatomic, strong) HXMaintVehicleTypeModel *vehicleTypeModel;
@property (nonatomic, strong) HXMaintChauffeurStatusModel *chaufferStatusArray;


//Array of self:
//@property (nonatomic, strong) NSMutableArray *vehicleListAry; // To store recieved details

//Operational Methods:
-(void)getMaintenanceData;


-(NSArray *)fetchAllRequestListFromDB;
-(NSArray *)fetchAllJobStatusListFromDB;
-(NSArray *)fetchAllSundriesListFromDB;
-(NSArray *)fetchAllVehicleListFromDB;



@end



