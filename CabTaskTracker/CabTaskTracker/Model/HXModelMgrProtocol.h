//
//  HXModelMgrProtocol.h
//  CabTaskTracker
//
//  Created by Mithun on 28/07/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HXModelMgrProtocol <NSObject>

@optional
-(void)saveCredentialInDB;
-(void)authenticationErrorDispaly;

-(void)storeVehicleListInDBWithMessage:(NSString *)message;

-(void)storeMaintenanceList_Status:(NSString *)message;
-(void)storeOpsList_Status:(NSString *)message;

@end
