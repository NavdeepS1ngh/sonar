//
//  HXBookingSundryModel.h
//  CabTaskTracker
//
//  Created by Mithun on 06/08/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HXBookingSundryModel : NSObject

@property (nonatomic, strong) NSString *sundryID;
@property (nonatomic, strong) NSString *refBooking;
@property (nonatomic, strong) NSString *refSundry;
@property (nonatomic, strong) NSString *amount;

@end
