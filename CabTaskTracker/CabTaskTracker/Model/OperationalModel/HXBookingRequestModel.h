//
//  HXBookingRequestModel.h
//  CabTaskTracker
//
//  Created by Mithun on 06/08/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HXBookingRequestModel : NSObject

@property (nonatomic, strong) NSString *requestID;
@property (nonatomic, strong) NSString *refBooking;
@property (nonatomic, strong) NSString *refRequest;
@property (nonatomic, strong) NSString *isActive;

@end
