//
//  HXBookingPassengerModel.h
//  CabTaskTracker
//
//  Created by Mithun on 06/08/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HXBookingPassengerModel : NSObject

@property (nonatomic, strong) NSString *passengerID;
@property (nonatomic, strong) NSString *refBooking;
@property (nonatomic, strong) NSString *refPassenger;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *isActive;

@end
