//
//  HXOperationalModel.h
//  CabTaskTracker
//
//  Created by Mithun on 06/08/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "HXBookingModel.h"
#import "HXBookingPassengerModel.h"
#import "HXBookingRequestModel.h"
#import "HXBookingLegsModel.h"
#import "HXBookingSundryModel.h"

#import "HXServiceMgrProtocol.h"
#import "HXModelMgrProtocol.h"


@interface HXOperationalModel : NSObject <HXServiceMgrProtocol>

@property(readwrite,assign)id<HXModelMgrProtocol>modelDelegate;

@property (nonatomic, strong) NSMutableArray *bookingArray;
@property (nonatomic, strong) NSMutableArray *bkLegsArray;
@property (nonatomic, strong) NSMutableArray *bkPassengerArray;
@property (nonatomic, strong) NSMutableArray *bkRequestArray;
@property (nonatomic, strong) NSMutableArray *bkSundryArray;

@property (nonatomic, strong) HXBookingModel *bookingModel;
@property (nonatomic, strong) HXBookingLegsModel *bkLegsModel;
@property (nonatomic, strong) HXBookingPassengerModel *bkPassengerModel;
@property (nonatomic, strong) HXBookingRequestModel *bkRequestModel;
@property (nonatomic, strong) HXBookingSundryModel *bkSundryModel;

@property (nonatomic, strong) NSMutableArray *bookingsToRemove;

-(void)getOpsData;

@end
