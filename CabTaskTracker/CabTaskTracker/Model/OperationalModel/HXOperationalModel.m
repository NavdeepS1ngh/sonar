//
//  HXOperationalModel.m
//  CabTaskTracker
//
//  Created by Mithun on 06/08/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import "HXOperationalModel.h"
#import "HXCoreDataManager.h"
#import "HXMainModel.h"

#import "HXWebServiceManager.h"

@implementation HXOperationalModel
{
    NSManagedObjectContext *context;
    NSEntityDescription *entityDesc;
    HXCoreDataManager* coreDataManager;
    
    HXWebServiceManager *serviceManager;
    
}
-(void)getOpsData
{
    serviceManager = [[HXWebServiceManager alloc]init];
    [serviceManager setServiceDelegate:self];
    [serviceManager getOpsDataRequest];
}

-(void)storeRecieviedDataInDB
{
    //Store the available data in model to DB:
    //To be executed after sucesfully loaded data from api:
    //CoreData Manager
    coreDataManager = [HXCoreDataManager sharedInstance];
    context = [coreDataManager managedObjectContext];
    
    for (HXBookingModel* obj in self.bookingArray) {
        
        NSString *predicateString = [NSString stringWithFormat:@"bookingID==\"%@\"", obj.bookingID];
        
        NSArray* results = [[HXCoreDataManager sharedInstance]getMatchedRowsFromEntity:@"Bookings" predicateString:predicateString];
        
        if (results.count > 0) {
            //updation
            NSManagedObject* tblRow = [results objectAtIndex:0 ];
        
        [tblRow setValue:obj.accountName forKey:@"accountName"];
        [tblRow setValue:obj.accountNumber forKey:@"accountNumber"];
        [tblRow setValue:obj.allowKMS forKey:@"allowKm"];
        [tblRow setValue:obj.allowSundries forKey:@"allowSundries"];
        [tblRow setValue:obj.bookingDate forKey:@"bookingDate"];
        [tblRow setValue:obj.bookingNumber forKey:@"bookingNumber"];
        [tblRow setValue:obj.drAddress forKey:@"drAddress"];
        [tblRow setValue:obj.drSuburb forKey:@"drSuburb"];
        [tblRow setValue:obj.isDispatched forKey:@"isDispatched"];
        [tblRow setValue:obj.pickupTime forKey:@"pickupTime"];
        [tblRow setValue:obj.puAddress forKey:@"puAddress"];
        [tblRow setValue:obj.puSuburb forKey:@"puSuburb"];
        [tblRow setValue:obj.refChauffeur forKey:@"refChauffeur"];
        [tblRow setValue:obj.refVehicleType forKey:@"refVehicleType"];
        [tblRow setValue:obj.specialInstructions forKey:@"specialInstructions"];
        [tblRow setValue:obj.totalKMS forKey:@"totalKM"];
        [tblRow setValue:obj.waitingTime forKey:@"waitingTime"];
        
        [context save:nil];
        }
        else
        {//insertion
            NSManagedObject* tblRow = [NSEntityDescription insertNewObjectForEntityForName:@"Bookings" inManagedObjectContext:context];
            
            [tblRow setValue:obj.accountName forKey:@"accountName"];
            [tblRow setValue:obj.accountNumber forKey:@"accountNumber"];
            [tblRow setValue:obj.allowKMS forKey:@"allowKm"];
            [tblRow setValue:obj.allowSundries forKey:@"allowSundries"];
            [tblRow setValue:obj.bookingDate forKey:@"bookingDate"];
            [tblRow setValue:obj.bookingNumber forKey:@"bookingNumber"];
            [tblRow setValue:obj.drAddress forKey:@"drAddress"];
            [tblRow setValue:obj.drSuburb forKey:@"drSuburb"];
            [tblRow setValue:obj.isDispatched forKey:@"isDispatched"];
            [tblRow setValue:obj.pickupTime forKey:@"pickupTime"];
            [tblRow setValue:obj.puAddress forKey:@"puAddress"];
            [tblRow setValue:obj.puSuburb forKey:@"puSuburb"];
            [tblRow setValue:obj.refChauffeur forKey:@"refChauffeur"];
            [tblRow setValue:obj.refVehicleType forKey:@"refVehicleType"];
            [tblRow setValue:obj.specialInstructions forKey:@"specialInstructions"];
            [tblRow setValue:obj.totalKMS forKey:@"totalKM"];
            [tblRow setValue:obj.waitingTime forKey:@"waitingTime"];
            [tblRow setValue:obj.bookingID forKey:@"bookingID"];

            [context save:nil];
        }
        
    }
    
    for (HXBookingLegsModel* obj in self.bkLegsArray) {
        NSString *predicateString = [NSString stringWithFormat:@"legsID==\"%@\"", obj.legsID];
        
        NSArray* results = [[HXCoreDataManager sharedInstance]getMatchedRowsFromEntity:@"Booking_Legs" predicateString:predicateString];
        
        if (results.count > 0) {
            //updation
            NSManagedObject* tblRow = [results objectAtIndex:0 ];
            
            [tblRow setValue:obj.flightNumber forKey:@"flightNumber"];
            [tblRow setValue:obj.isActive forKey:@"isActive"];
            [tblRow setValue:obj.legAddress forKey:@"legAddress"];
            [tblRow setValue:obj.legNumber forKey:@"legNumber"];
            [tblRow setValue:obj.legPhone forKey:@"legPhone"];
            [tblRow setValue:obj.legSuburb forKey:@"legSuburb"];
            [tblRow setValue:obj.refBooking forKey:@"refBooking"];
            
            [context save:nil];
        }else
        {
            //inseriton
            
        NSManagedObject* tblRow = [NSEntityDescription insertNewObjectForEntityForName:@"Booking_Legs" inManagedObjectContext:context];
        
        [tblRow setValue:obj.flightNumber forKey:@"flightNumber"];
        [tblRow setValue:obj.isActive forKey:@"isActive"];
        [tblRow setValue:obj.legAddress forKey:@"legAddress"];
        [tblRow setValue:obj.legNumber forKey:@"legNumber"];
        [tblRow setValue:obj.legPhone forKey:@"legPhone"];
        [tblRow setValue:obj.legsID forKey:@"legsID"];
        [tblRow setValue:obj.legSuburb forKey:@"legSuburb"];
        [tblRow setValue:obj.refBooking forKey:@"refBooking"];

        [context save:nil];
        }
    }
    
    for (HXBookingPassengerModel* obj in self.bkPassengerArray) {
        NSString *predicateString = [NSString stringWithFormat:@"passengerID==\"%@\"", obj.passengerID];
        
        NSArray* results = [[HXCoreDataManager sharedInstance]getMatchedRowsFromEntity:@"Booking_Passenger" predicateString:predicateString];
        
        if (results.count > 0) {
            //updation
            NSManagedObject* tblRow = [results objectAtIndex:0 ];
            [tblRow setValue:obj.firstName forKey:@"firstName"];
            [tblRow setValue:obj.isActive forKey:@"isActive"];
            [tblRow setValue:obj.lastName forKey:@"lastName"];
            [tblRow setValue:obj.passengerID forKey:@"passengerID"];
            [tblRow setValue:obj.refBooking forKey:@"refBooking"];
            [tblRow setValue:obj.refPassenger forKey:@"refPassenger"];
            
            [context save:nil];

        }
        else{//insertion
        NSManagedObject* tblRow = [NSEntityDescription insertNewObjectForEntityForName:@"Booking_Passenger" inManagedObjectContext:context];
        
        [tblRow setValue:obj.firstName forKey:@"firstName"];
        [tblRow setValue:obj.isActive forKey:@"isActive"];
        [tblRow setValue:obj.lastName forKey:@"lastName"];
        [tblRow setValue:obj.passengerID forKey:@"passengerID"];
        [tblRow setValue:obj.refBooking forKey:@"refBooking"];
        [tblRow setValue:obj.refPassenger forKey:@"refPassenger"];

        [context save:nil];
        }
    }
    
    for (HXBookingRequestModel* obj in self.bkRequestArray) {
        NSString *predicateString = [NSString stringWithFormat:@"requestID==\"%@\"", obj.requestID];
        
        NSArray* results = [[HXCoreDataManager sharedInstance]getMatchedRowsFromEntity:@"Booking_Request" predicateString:predicateString];
        
        if (results.count > 0) {
            //updation
            NSManagedObject* tblRow = [results objectAtIndex:0 ];
            
            [tblRow setValue:obj.isActive forKey:@"isActive"];
            [tblRow setValue:obj.refBooking forKey:@"refBooking"];
            [tblRow setValue:obj.refRequest forKey:@"refRequest"];
            
            [context save:nil];

        }
        else
        {
        NSManagedObject* tblRow = [NSEntityDescription insertNewObjectForEntityForName:@"Booking_Request" inManagedObjectContext:context];
        
        [tblRow setValue:obj.isActive forKey:@"isActive"];
        [tblRow setValue:obj.refBooking forKey:@"refBooking"];
        [tblRow setValue:obj.refRequest forKey:@"refRequest"];
        [tblRow setValue:obj.requestID forKey:@"requestID"];
        
        [context save:nil];
        }
    }
    
    for (HXBookingSundryModel* obj in self.bkSundryArray) {
        NSString *predicateString = [NSString stringWithFormat:@"sundryID==\"%@\"", obj.sundryID];
        
        NSArray* results = [[HXCoreDataManager sharedInstance]getMatchedRowsFromEntity:@"Booking_Sundries" predicateString:predicateString];
        
        if (results.count > 0) {
            //updation
            NSManagedObject* tblRow = [results objectAtIndex:0 ];
            
            [tblRow setValue:obj.amount forKey:@"amount"];
            [tblRow setValue:obj.refBooking forKey:@"refBooking"];
            [tblRow setValue:obj.refSundry forKey:@"refSundry"];
            
            [context save:nil];

        }
        else
        {
        NSManagedObject* tblRow = [NSEntityDescription insertNewObjectForEntityForName:@"Booking_Sundries" inManagedObjectContext:context];
        
        [tblRow setValue:obj.amount forKey:@"amount"];
        [tblRow setValue:obj.refBooking forKey:@"refBooking"];
        [tblRow setValue:obj.refSundry forKey:@"refSundry"];
        [tblRow setValue:obj.sundryID forKey:@"sundryID"];

        [context save:nil];
        }
        
    }
    
    //Call HXModelMgrProtocol with sucess message:
    [_modelDelegate storeOpsList_Status:@"OK"];
    
}


-(void)retriveDataToModelFromDB
{
    //retrieve data to model once records have been updated in model:
    //TO be called in offline login mode and after update in DB from webAPI
}


#pragma mark - Protocol Implementation: HXWebServiceProtocol
-(void)recievedOpsData:(NSDictionary *)opsDict
{
    
    NSArray *recievedBookingArray = [opsDict objectForKey:@"bookings"];
    _bookingArray = [[NSMutableArray alloc]init];
    for (NSDictionary *tempDict in recievedBookingArray) {
        
        _bookingModel = [[HXBookingModel alloc]init];
        
        _bookingModel.bookingID = [NSString stringWithFormat:@"%@",[tempDict valueForKey:@"id"]];
        _bookingModel.refChauffeur = [NSString stringWithFormat:@"%@",[tempDict valueForKey:@"refChauffeur"]];
        _bookingModel.refVehicleType =[NSString stringWithFormat:@"%@", [tempDict valueForKey:@"refVehicleType"]];
        _bookingModel.bookingNumber = [tempDict valueForKey:@"bookingNumber"];
        _bookingModel.bookingDate = [tempDict valueForKey:@"bookingDate"];
        _bookingModel.pickupTime = [tempDict valueForKey:@"pickupTime"];
        _bookingModel.accountNumber = [tempDict valueForKey:@"accountNumber"];
        _bookingModel.accountName = [tempDict valueForKey:@"accountName"];
        _bookingModel.puSuburb = [tempDict valueForKey:@"pU_Suburb"];
        _bookingModel.puAddress = [tempDict valueForKey:@"pU_Address"];
        _bookingModel.drSuburb = [tempDict valueForKey:@"dr_Suburb"];
        _bookingModel.drAddress = [tempDict valueForKey:@"dr_Address"];
        _bookingModel.specialInstructions =[NSString stringWithFormat:@"%@", [tempDict valueForKey:@"specialInstructions"]];
        _bookingModel.isDispatched = [NSString stringWithFormat:@"%@",[tempDict valueForKey:@"isDespatched"]];
        _bookingModel.allowSundries =[NSString stringWithFormat:@"%@", [tempDict valueForKey:@"allowSundries"]];
        _bookingModel.allowKMS =[tempDict valueForKey:@"allowKM"];
        _bookingModel.totalKMS = [tempDict valueForKey:@"totalKMS"];
        _bookingModel.waitingTime = [NSString stringWithFormat:@"%@",[tempDict valueForKey:@"waitingTimeMin"]];
        
        [_bookingArray addObject:_bookingModel];
        
    }
    
    NSArray *recievedBkLegsArray = [opsDict objectForKey:@"bookingLegs"];
    _bkLegsArray = [[NSMutableArray alloc]init];
    for (NSDictionary *tempDict in recievedBkLegsArray) {
        
        _bkLegsModel = [[HXBookingLegsModel alloc]init];
        
        _bkLegsModel.legsID = [NSString stringWithFormat:@"%@",[tempDict valueForKey:@"id"]];
        _bkLegsModel.refBooking = [NSString stringWithFormat:@"%@",[tempDict valueForKey:@"refBooking"]];
        _bkLegsModel.legNumber = [NSString stringWithFormat:@"%@",[tempDict valueForKey:@"legNumber"]];
        _bkLegsModel.legAddress = [tempDict valueForKey:@"legAddress"];
        _bkLegsModel.legSuburb = [tempDict valueForKey:@"legSuburb"];
        _bkLegsModel.legPhone = [tempDict valueForKey:@"legPhone"];
        _bkLegsModel.flightNumber = [tempDict valueForKey:@"flightNo"];
        _bkLegsModel.isActive = [NSString stringWithFormat:@"%@",[tempDict valueForKey:@"isActive"]];
        
        
        [_bkLegsArray addObject:_bkLegsModel];
        
    }
    
    NSArray *recievedBkPassengersArray = [opsDict objectForKey:@"bookingPassengers"];
    _bkPassengerArray = [[NSMutableArray alloc]init];
    for (NSDictionary *tempDict in recievedBkPassengersArray) {
        
        _bkPassengerModel = [[HXBookingPassengerModel alloc]init];
        
        _bkPassengerModel.passengerID = [NSString stringWithFormat:@"%@",[tempDict valueForKey:@"id"]];
        _bkPassengerModel.refBooking = [NSString stringWithFormat:@"%@",[tempDict valueForKey:@"refBooking"]];
        _bkPassengerModel.refPassenger = [NSString stringWithFormat:@"%@",[tempDict valueForKey:@"refPassenger"]];
        _bkPassengerModel.firstName = [tempDict valueForKey:@"firstName"];
        _bkPassengerModel.lastName = [tempDict valueForKey:@"lastName"];
        _bkPassengerModel.isActive = [NSString stringWithFormat:@"%@",[tempDict valueForKey:@"isActive"]];
        
        
        [_bkPassengerArray addObject:_bkPassengerModel];
        
    }
    
    NSArray *recievedBkRequestArray = [opsDict objectForKey:@"bookingRequests"];
    _bkRequestArray = [[NSMutableArray alloc]init];
    for (NSDictionary *tempDict in recievedBkRequestArray) {
        
        _bkRequestModel = [[HXBookingRequestModel alloc]init];
        
        _bkRequestModel.requestID = [NSString stringWithFormat:@"%@",[tempDict valueForKey:@"id"]];
        _bkRequestModel.refBooking =[NSString stringWithFormat:@"%@",[tempDict valueForKey:@"refBooking"]];
        _bkRequestModel.refRequest = [NSString stringWithFormat:@"%@",[tempDict valueForKey:@"refRequest"]];
        _bkRequestModel.isActive = [NSString stringWithFormat:@"%@",[tempDict valueForKey:@"isActive"]];
        
        [_bkRequestArray addObject:_bkRequestModel];
        
    }
    
    NSArray *recievedBkSundriesArray = [opsDict objectForKey:@"bookingSundries"];
    _bkSundryArray = [[NSMutableArray alloc]init];
    for (NSDictionary *tempDict in recievedBkSundriesArray) {
        
        _bkSundryModel = [[HXBookingSundryModel alloc]init];
        
        _bkSundryModel.sundryID = [NSString stringWithFormat:@"%@",[tempDict valueForKey:@"id"]];
        _bkSundryModel.refBooking = [NSString stringWithFormat:@"%@",[tempDict valueForKey:@"refBooking"]];
        _bkSundryModel.refSundry = [NSString stringWithFormat:@"%@",[tempDict valueForKey:@"refSundry"]];
        //_bkSundryModel.amount = [tempDict valueForKey:@"asdasd"];
        
        [_bkSundryArray addObject:_bkSundryModel];
        
    }
    
    _bookingsToRemove = [opsDict valueForKey:@"bookingsToRemove"];
    if (_bookingsToRemove.count > 0) {
        //Remove those bookings from DB
    }
    
    [HXMainModel saveSyncOpsDateKey:[opsDict valueForKey:@"syncKey"]];
    
    
    //CAll store details in DB:
    [self storeRecieviedDataInDB];
    
    
}

-(void)recievedOpsFailedWithReason:(NSString *)reason
{
    NSLog(@"Operational Data recieved failure");
    [_modelDelegate storeOpsList_Status:reason];
}

@end
