//
//  HXMainModel.h
//  CabTaskTracker
//
//  Created by Mithun on 21/07/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Reachability.h"

#import "HXAuthenticationModel.h"
#import "HXVehicleListModel.h"
#import "HXMaintenanceModel.h"
#import "HXOperationalModel.h"

#import "HXJobDetailsModel.h"
#import "HXAdditionalJobDetailsModel.h"

#import "HXControllerMgrProtocol.h"

@interface HXMainModel : NSObject <HXModelMgrProtocol>

@property(readwrite,assign)id<HXControllerMgrProtocol>controllerDelegate;

@property (nonatomic, strong) Reachability *internetReachability;
@property BOOL isNetworkConnected;
@property BOOL isServerConnected;
@property BOOL isValidToken;

@property BOOL isOfflineLogin;
@property BOOL isMaintenanceDataAvailable;
@property BOOL isVehicleListCallPending;

@property int jobDetailArrayIndex;
@property (nonatomic, strong) NSString *currentUser;
@property (strong, nonatomic) NSString *pswDcryptKey;


//SubModel Class
@property (nonatomic, strong) HXAuthenticationModel *authenticateModel;
@property (nonatomic, strong) HXVehicleListModel *vehicleModel;
@property (nonatomic, strong) HXMaintenanceModel *maintenanceModel;
@property (nonatomic, strong) HXOperationalModel *operationalModel;
@property (nonatomic, strong) HXJobDetailsModel *jobDetailsModel;

@property (nonatomic, strong) HXAdditionalJobDetailsModel *additionalJobDetailsModel;

@property (nonatomic, strong) NSString *selectedRefBookingID;

@property (nonatomic, strong) NSMutableArray *jobDetailsList;

@property (nonatomic, strong) NSArray *allJobStatusList;
@property (nonatomic, strong) NSArray *allSundriesList;
@property (nonatomic, strong) NSArray *allAdditionalRequestList;
@property (nonatomic, strong) NSArray *allVehicleList;




+(HXMainModel*)sharedInstance;

//Util Methods:
+(void)saveSyncMaintenanceKey:(NSString*)syncKey;
+(NSString *)syncMaintenanceKey;

+(void)saveSyncOpsDateKey:(NSString*)syncKey;
+(NSString *)syncOpsDateKey;

+(void)saveNewToken:(NSString*)recievedToken;
+(NSString *)currentToken;

+(NSString*)getEncryptedPasswordFor:(NSString*)pswString;

//Rechablity Methods
-(void)initiateNetworkTracker;
-(void)stopNetworkTracker;

//Model Operation Methods

-(void)getVehicleList;
-(NSMutableArray*) getVehicleIDFromDB;

-(void)getMaintenanceData;
-(void)getOperationalData;

-(void)fetchDataForJobDetails;
-(void)fetchLegAndAddtionalRequestForBooking:(NSString *)refBooking;
-(void)fetchPredefinedListsFromMaintenanceDB;






@end

