//
//  main.m
//  CabTaskTracker
//
//  Created by Mithun R on 26/06/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HXAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HXAppDelegate class]));
    }
}
