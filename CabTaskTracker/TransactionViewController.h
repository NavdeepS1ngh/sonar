//
//  TransactionViewController.h
//  CabTaskTracker
//
//  Created by Navdeep  Singh on 8/12/14.
//  Copyright (c) 2014 Hexaware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransactionViewController : UIViewController<UIAlertViewDelegate>

@end
